## Installation

    composer require zwei14/openimmo:dev-master

## Usage

Install composer package.

## (Re) Generate classes for new OpenImmo version

### OpenImmo 

    ./../../../vendor/bin/xsd2php convert openimmo.yml path/to/openimmo_127b.xsd
    git apply patch_user_defined_simplefield.patch
    composer dumpautoload

Patch fixes user_defined_simplefield class and corresponding *.yml file for JMS serializer. Otherwise values are missing in XML after reading or writing (e.g. in import).

### OpenImmo-Feedback

    ./../../../vendor/bin/xsd2php convert openimmo_feedback.yml path/to/openimmo-feedback_125.xsd
    composer dumpautoload

## Reading OpenImmo XML

    $xmlString = file_get_contents('path/to/foobar.xml');

    $serializerBuilder = SerializerBuilder::create();
    $serializerBuilder->addMetadataDir(
        __DIR__ . '/../../../../../../vendor/zwei14/openimmo/metadata/Zwei14/OpenImmo/API',     
        'Zwei14\OpenImmo\API'
    );

    $serializerBuilder->configureHandlers(function (HandlerRegistryInterface $handler) use ($serializerBuilder) {
        $serializerBuilder->addDefaultHandlers();
        $handler->registerSubscribingHandler(new BaseTypesHandler()); // XMLSchema List handling
        $handler->registerSubscribingHandler(new XmlSchemaDateHandler()); // XMLSchema date handling
        // $handler->registerSubscribingHandler(new YourhandlerHere());
    });

    $serializer = $serializerBuilder->build();

    /* @var $openImmo Openimmo */
    $openImmo = $serializer->deserialize($xmlString, Openimmo::class, 'xml');

## Writing OpenImmo XML

    $infrastruktur = new Infrastruktur();
    
    $ausblick = new Ausblick();
    $ausblick->setBlick('BERGE');

    $distanzenSport = [
        new DistanzenSport(15.0),
        new DistanzenSport(10.0),
    ];
    $distanzenSport[0]->setDistanzZuSport('SEE');
    $distanzenSport[1]->setDistanzZuSport('SPORTANLAGEN');

    $distanzen = [
        new Distanzen(1.0),
    ];
    $distanzen[0]->setDistanzZu('AUTOBAHN');

    $infrastruktur
        ->setZulieferung(false)
        ->setAusblick($ausblick)
        ->setDistanzenSport($distanzenSport)
        ->setDistanzen($distanzen);

    $newXml = $serializer->serialize($infrastruktur, 'xml');

### Ergebnis

    <infrastruktur>
        <zulieferung>false</zulieferung>
            <ausblick blick="BERGE"/>
            <distanzen distanz_zu="AUTOBAHN">1.0</distanzen>
            <distanzen_sport distanz_zu_sport="SEE">15.0</distanzen_sport>
            <distanzen_sport distanz_zu_sport="SPORTANLAGEN">10.0</distanzen_sport>
    </infrastruktur>