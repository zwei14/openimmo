<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\Sonstigekostennetto\SonstigekostennettoAType;

/**
 * Class representing Sonstigekostennetto
 *
 * Anganen bei Miet-Objekten, UmSt. im Attribut.
 */
class Sonstigekostennetto extends SonstigekostennettoAType
{


}

