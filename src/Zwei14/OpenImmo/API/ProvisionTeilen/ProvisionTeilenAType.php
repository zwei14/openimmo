<?php

namespace Zwei14\OpenImmo\API\ProvisionTeilen;

/**
 * Class representing ProvisionTeilenAType
 */
class ProvisionTeilenAType
{

    /**
     * @var string $wert
     */
    private $wert = null;

    /**
     * Gets as wert
     *
     * @return string
     */
    public function getWert()
    {
        return $this->wert;
    }

    /**
     * Sets a new wert
     *
     * @param string $wert
     * @return self
     */
    public function setWert($wert)
    {
        $this->wert = $wert;
        return $this;
    }


}

