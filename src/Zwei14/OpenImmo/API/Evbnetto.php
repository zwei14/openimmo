<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\Evbnetto\EvbnettoAType;

/**
 * Class representing Evbnetto
 *
 * Erhaltungs- und Verbesserungsbeitrag. Ähnlich Instanthaltungsrücklage, UmSt. im Attribut.
 */
class Evbnetto extends EvbnettoAType
{


}

