<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\Gesamtmietenetto\GesamtmietenettoAType;

/**
 * Class representing Gesamtmietenetto
 *
 * Summe alle Mietzins Zahlungen, UmSt. im Attribut.
 */
class Gesamtmietenetto extends GesamtmietenettoAType
{


}

