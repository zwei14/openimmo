<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\Versteigerung\VersteigerungAType;

/**
 * Class representing Versteigerung
 *
 * Angaben zu einer Versteigerung. Wenn es ein Objekt in Zwangsverteigerung ist, dann muss das element "zwangsversteigerung" auf true/1 gesetzt werden.
 */
class Versteigerung extends VersteigerungAType
{


}

