<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\MinMietdauer\MinMietdauerAType;

/**
 * Class representing MinMietdauer
 *
 * Mindestzeitraum für den die Immobilie gemietet werden muss, Optionen nicht kombinierbar, vorrangig bei WaZ
 */
class MinMietdauer extends MinMietdauerAType
{


}

