<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\Sonstigemietenetto\SonstigemietenettoAType;

/**
 * Class representing Sonstigemietenetto
 *
 * Ergänzenden Mietkosten, UmSt. im Attribut.
 */
class Sonstigemietenetto extends SonstigemietenettoAType
{


}

