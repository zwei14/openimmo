<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\Aktion\AktionAType;

/**
 * Class representing Aktion
 *
 * Aktion für Objekt. Wenn nicht vorhanden, dann "ADD", als neu.
 *  Change= Update der Objektdaten, Delete = Löschen des Objektes
 *  Referenz= Die Möglichkeit Objekte in Portalen als Verkauft oder Archiv zu definieren.
 */
class Aktion extends AktionAType
{


}

