<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\Stellplatzart\StellplatzartAType;

/**
 * Class representing Stellplatzart
 *
 * Welche Stellplatzarten sind vorhanden, Optionen kombinierbar, als einfache Alternative zu den stp...Elementen
 */
class Stellplatzart extends StellplatzartAType
{


}

