<?php

namespace Zwei14\OpenImmo\API\Sonstige;

/**
 * Class representing SonstigeAType
 */
class SonstigeAType
{

    /**
     * @var string $sonstigeTyp
     */
    private $sonstigeTyp = null;

    /**
     * Gets as sonstigeTyp
     *
     * @return string
     */
    public function getSonstigeTyp()
    {
        return $this->sonstigeTyp;
    }

    /**
     * Sets a new sonstigeTyp
     *
     * @param string $sonstigeTyp
     * @return self
     */
    public function setSonstigeTyp($sonstigeTyp)
    {
        $this->sonstigeTyp = $sonstigeTyp;
        return $this;
    }


}

