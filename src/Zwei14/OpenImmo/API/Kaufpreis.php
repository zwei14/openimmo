<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\Kaufpreis\KaufpreisAType;

/**
 * Class representing Kaufpreis
 *
 * Gesamt- (Angebots-)Kaufpreis der Immobilie. Wenn "Auf Anfrage" dann Wert = 0 und Attribut auf TRUE
 */
class Kaufpreis extends KaufpreisAType
{


}

