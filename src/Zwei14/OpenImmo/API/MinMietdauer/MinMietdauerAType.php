<?php

namespace Zwei14\OpenImmo\API\MinMietdauer;

/**
 * Class representing MinMietdauerAType
 */
class MinMietdauerAType
{

    /**
     * @var string $minDauer
     */
    private $minDauer = null;

    /**
     * Gets as minDauer
     *
     * @return string
     */
    public function getMinDauer()
    {
        return $this->minDauer;
    }

    /**
     * Sets a new minDauer
     *
     * @param string $minDauer
     * @return self
     */
    public function setMinDauer($minDauer)
    {
        $this->minDauer = $minDauer;
        return $this;
    }


}

