<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\ObjektText\ObjektTextAType;

/**
 * Class representing ObjektText
 *
 * Beschreibung in anderer Sprache. "lang" Attribut muss dann vorhanden sein. W3- Language Code Description in other Languages
 */
class ObjektText extends ObjektTextAType
{


}

