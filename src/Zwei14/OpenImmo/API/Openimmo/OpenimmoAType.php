<?php

namespace Zwei14\OpenImmo\API\Openimmo;

/**
 * Class representing OpenimmoAType
 */
class OpenimmoAType
{

    /**
     * @var \Zwei14\OpenImmo\API\Uebertragung $uebertragung
     */
    private $uebertragung = null;

    /**
     * @var \Zwei14\OpenImmo\API\Anbieter[] $anbieter
     */
    private $anbieter = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\UserDefinedSimplefield[] $userDefinedSimplefield
     */
    private $userDefinedSimplefield = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\UserDefinedAnyfield[] $userDefinedAnyfield
     */
    private $userDefinedAnyfield = [
        
    ];

    /**
     * Gets as uebertragung
     *
     * @return \Zwei14\OpenImmo\API\Uebertragung
     */
    public function getUebertragung()
    {
        return $this->uebertragung;
    }

    /**
     * Sets a new uebertragung
     *
     * @param \Zwei14\OpenImmo\API\Uebertragung $uebertragung
     * @return self
     */
    public function setUebertragung(\Zwei14\OpenImmo\API\Uebertragung $uebertragung)
    {
        $this->uebertragung = $uebertragung;
        return $this;
    }

    /**
     * Adds as anbieter
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\Anbieter $anbieter
     */
    public function addToAnbieter(\Zwei14\OpenImmo\API\Anbieter $anbieter)
    {
        $this->anbieter[] = $anbieter;
        return $this;
    }

    /**
     * isset anbieter
     *
     * @param int|string $index
     * @return bool
     */
    public function issetAnbieter($index)
    {
        return isset($this->anbieter[$index]);
    }

    /**
     * unset anbieter
     *
     * @param int|string $index
     * @return void
     */
    public function unsetAnbieter($index)
    {
        unset($this->anbieter[$index]);
    }

    /**
     * Gets as anbieter
     *
     * @return \Zwei14\OpenImmo\API\Anbieter[]
     */
    public function getAnbieter()
    {
        return $this->anbieter;
    }

    /**
     * Sets a new anbieter
     *
     * @param \Zwei14\OpenImmo\API\Anbieter[] $anbieter
     * @return self
     */
    public function setAnbieter(array $anbieter)
    {
        $this->anbieter = $anbieter;
        return $this;
    }

    /**
     * Adds as userDefinedSimplefield
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\UserDefinedSimplefield $userDefinedSimplefield
     */
    public function addToUserDefinedSimplefield(\Zwei14\OpenImmo\API\UserDefinedSimplefield $userDefinedSimplefield)
    {
        $this->userDefinedSimplefield[] = $userDefinedSimplefield;
        return $this;
    }

    /**
     * isset userDefinedSimplefield
     *
     * @param int|string $index
     * @return bool
     */
    public function issetUserDefinedSimplefield($index)
    {
        return isset($this->userDefinedSimplefield[$index]);
    }

    /**
     * unset userDefinedSimplefield
     *
     * @param int|string $index
     * @return void
     */
    public function unsetUserDefinedSimplefield($index)
    {
        unset($this->userDefinedSimplefield[$index]);
    }

    /**
     * Gets as userDefinedSimplefield
     *
     * @return \Zwei14\OpenImmo\API\UserDefinedSimplefield[]
     */
    public function getUserDefinedSimplefield()
    {
        return $this->userDefinedSimplefield;
    }

    /**
     * Sets a new userDefinedSimplefield
     *
     * @param \Zwei14\OpenImmo\API\UserDefinedSimplefield[] $userDefinedSimplefield
     * @return self
     */
    public function setUserDefinedSimplefield(array $userDefinedSimplefield)
    {
        $this->userDefinedSimplefield = $userDefinedSimplefield;
        return $this;
    }

    /**
     * Adds as userDefinedAnyfield
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\UserDefinedAnyfield $userDefinedAnyfield
     */
    public function addToUserDefinedAnyfield(\Zwei14\OpenImmo\API\UserDefinedAnyfield $userDefinedAnyfield)
    {
        $this->userDefinedAnyfield[] = $userDefinedAnyfield;
        return $this;
    }

    /**
     * isset userDefinedAnyfield
     *
     * @param int|string $index
     * @return bool
     */
    public function issetUserDefinedAnyfield($index)
    {
        return isset($this->userDefinedAnyfield[$index]);
    }

    /**
     * unset userDefinedAnyfield
     *
     * @param int|string $index
     * @return void
     */
    public function unsetUserDefinedAnyfield($index)
    {
        unset($this->userDefinedAnyfield[$index]);
    }

    /**
     * Gets as userDefinedAnyfield
     *
     * @return \Zwei14\OpenImmo\API\UserDefinedAnyfield[]
     */
    public function getUserDefinedAnyfield()
    {
        return $this->userDefinedAnyfield;
    }

    /**
     * Sets a new userDefinedAnyfield
     *
     * @param \Zwei14\OpenImmo\API\UserDefinedAnyfield[] $userDefinedAnyfield
     * @return self
     */
    public function setUserDefinedAnyfield(array $userDefinedAnyfield)
    {
        $this->userDefinedAnyfield = $userDefinedAnyfield;
        return $this;
    }


}

