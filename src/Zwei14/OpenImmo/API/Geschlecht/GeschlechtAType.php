<?php

namespace Zwei14\OpenImmo\API\Geschlecht;

/**
 * Class representing GeschlechtAType
 */
class GeschlechtAType
{

    /**
     * @var string $geschlAttr
     */
    private $geschlAttr = null;

    /**
     * Gets as geschlAttr
     *
     * @return string
     */
    public function getGeschlAttr()
    {
        return $this->geschlAttr;
    }

    /**
     * Sets a new geschlAttr
     *
     * @param string $geschlAttr
     * @return self
     */
    public function setGeschlAttr($geschlAttr)
    {
        $this->geschlAttr = $geschlAttr;
        return $this;
    }


}

