<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\Heizkostennetto\HeizkostennettoAType;

/**
 * Class representing Heizkostennetto
 *
 * Die Heizkosten einer Einheit als Nettowert. Die Umsatzsteuer optional im Attribut
 */
class Heizkostennetto extends HeizkostennettoAType
{


}

