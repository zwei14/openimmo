<?php

namespace Zwei14\OpenImmo\API\ZustandAngaben;

/**
 * Class representing ZustandAngabenAType
 */
class ZustandAngabenAType
{

    /**
     * @var string $baujahr
     */
    private $baujahr = null;

    /**
     * @var string $letztemodernisierung
     */
    private $letztemodernisierung = null;

    /**
     * @var \Zwei14\OpenImmo\API\Zustand $zustand
     */
    private $zustand = null;

    /**
     * @var \Zwei14\OpenImmo\API\Alter $alter
     */
    private $alter = null;

    /**
     * @var \Zwei14\OpenImmo\API\BebaubarNach $bebaubarNach
     */
    private $bebaubarNach = null;

    /**
     * @var \Zwei14\OpenImmo\API\Erschliessung $erschliessung
     */
    private $erschliessung = null;

    /**
     * @var \Zwei14\OpenImmo\API\ErschliessungUmfang $erschliessungUmfang
     */
    private $erschliessungUmfang = null;

    /**
     * @var string $bauzone
     */
    private $bauzone = null;

    /**
     * @var string $altlasten
     */
    private $altlasten = null;

    /**
     * @var \Zwei14\OpenImmo\API\Energiepass[] $energiepass
     */
    private $energiepass = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\Verkaufstatus $verkaufstatus
     */
    private $verkaufstatus = null;

    /**
     * @var \Zwei14\OpenImmo\API\UserDefinedSimplefield[] $userDefinedSimplefield
     */
    private $userDefinedSimplefield = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\UserDefinedAnyfield[] $userDefinedAnyfield
     */
    private $userDefinedAnyfield = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[] $userDefinedExtend
     */
    private $userDefinedExtend = null;

    /**
     * Gets as baujahr
     *
     * @return string
     */
    public function getBaujahr()
    {
        return $this->baujahr;
    }

    /**
     * Sets a new baujahr
     *
     * @param string $baujahr
     * @return self
     */
    public function setBaujahr($baujahr)
    {
        $this->baujahr = $baujahr;
        return $this;
    }

    /**
     * Gets as letztemodernisierung
     *
     * @return string
     */
    public function getLetztemodernisierung()
    {
        return $this->letztemodernisierung;
    }

    /**
     * Sets a new letztemodernisierung
     *
     * @param string $letztemodernisierung
     * @return self
     */
    public function setLetztemodernisierung($letztemodernisierung)
    {
        $this->letztemodernisierung = $letztemodernisierung;
        return $this;
    }

    /**
     * Gets as zustand
     *
     * @return \Zwei14\OpenImmo\API\Zustand
     */
    public function getZustand()
    {
        return $this->zustand;
    }

    /**
     * Sets a new zustand
     *
     * @param \Zwei14\OpenImmo\API\Zustand $zustand
     * @return self
     */
    public function setZustand(\Zwei14\OpenImmo\API\Zustand $zustand)
    {
        $this->zustand = $zustand;
        return $this;
    }

    /**
     * Gets as alter
     *
     * @return \Zwei14\OpenImmo\API\Alter
     */
    public function getAlter()
    {
        return $this->alter;
    }

    /**
     * Sets a new alter
     *
     * @param \Zwei14\OpenImmo\API\Alter $alter
     * @return self
     */
    public function setAlter(\Zwei14\OpenImmo\API\Alter $alter)
    {
        $this->alter = $alter;
        return $this;
    }

    /**
     * Gets as bebaubarNach
     *
     * @return \Zwei14\OpenImmo\API\BebaubarNach
     */
    public function getBebaubarNach()
    {
        return $this->bebaubarNach;
    }

    /**
     * Sets a new bebaubarNach
     *
     * @param \Zwei14\OpenImmo\API\BebaubarNach $bebaubarNach
     * @return self
     */
    public function setBebaubarNach(\Zwei14\OpenImmo\API\BebaubarNach $bebaubarNach)
    {
        $this->bebaubarNach = $bebaubarNach;
        return $this;
    }

    /**
     * Gets as erschliessung
     *
     * @return \Zwei14\OpenImmo\API\Erschliessung
     */
    public function getErschliessung()
    {
        return $this->erschliessung;
    }

    /**
     * Sets a new erschliessung
     *
     * @param \Zwei14\OpenImmo\API\Erschliessung $erschliessung
     * @return self
     */
    public function setErschliessung(\Zwei14\OpenImmo\API\Erschliessung $erschliessung)
    {
        $this->erschliessung = $erschliessung;
        return $this;
    }

    /**
     * Gets as erschliessungUmfang
     *
     * @return \Zwei14\OpenImmo\API\ErschliessungUmfang
     */
    public function getErschliessungUmfang()
    {
        return $this->erschliessungUmfang;
    }

    /**
     * Sets a new erschliessungUmfang
     *
     * @param \Zwei14\OpenImmo\API\ErschliessungUmfang $erschliessungUmfang
     * @return self
     */
    public function setErschliessungUmfang(\Zwei14\OpenImmo\API\ErschliessungUmfang $erschliessungUmfang)
    {
        $this->erschliessungUmfang = $erschliessungUmfang;
        return $this;
    }

    /**
     * Gets as bauzone
     *
     * @return string
     */
    public function getBauzone()
    {
        return $this->bauzone;
    }

    /**
     * Sets a new bauzone
     *
     * @param string $bauzone
     * @return self
     */
    public function setBauzone($bauzone)
    {
        $this->bauzone = $bauzone;
        return $this;
    }

    /**
     * Gets as altlasten
     *
     * @return string
     */
    public function getAltlasten()
    {
        return $this->altlasten;
    }

    /**
     * Sets a new altlasten
     *
     * @param string $altlasten
     * @return self
     */
    public function setAltlasten($altlasten)
    {
        $this->altlasten = $altlasten;
        return $this;
    }

    /**
     * Adds as energiepass
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\Energiepass $energiepass
     */
    public function addToEnergiepass(\Zwei14\OpenImmo\API\Energiepass $energiepass)
    {
        $this->energiepass[] = $energiepass;
        return $this;
    }

    /**
     * isset energiepass
     *
     * @param int|string $index
     * @return bool
     */
    public function issetEnergiepass($index)
    {
        return isset($this->energiepass[$index]);
    }

    /**
     * unset energiepass
     *
     * @param int|string $index
     * @return void
     */
    public function unsetEnergiepass($index)
    {
        unset($this->energiepass[$index]);
    }

    /**
     * Gets as energiepass
     *
     * @return \Zwei14\OpenImmo\API\Energiepass[]
     */
    public function getEnergiepass()
    {
        return $this->energiepass;
    }

    /**
     * Sets a new energiepass
     *
     * @param \Zwei14\OpenImmo\API\Energiepass[] $energiepass
     * @return self
     */
    public function setEnergiepass(array $energiepass)
    {
        $this->energiepass = $energiepass;
        return $this;
    }

    /**
     * Gets as verkaufstatus
     *
     * @return \Zwei14\OpenImmo\API\Verkaufstatus
     */
    public function getVerkaufstatus()
    {
        return $this->verkaufstatus;
    }

    /**
     * Sets a new verkaufstatus
     *
     * @param \Zwei14\OpenImmo\API\Verkaufstatus $verkaufstatus
     * @return self
     */
    public function setVerkaufstatus(\Zwei14\OpenImmo\API\Verkaufstatus $verkaufstatus)
    {
        $this->verkaufstatus = $verkaufstatus;
        return $this;
    }

    /**
     * Adds as userDefinedSimplefield
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\UserDefinedSimplefield $userDefinedSimplefield
     */
    public function addToUserDefinedSimplefield(\Zwei14\OpenImmo\API\UserDefinedSimplefield $userDefinedSimplefield)
    {
        $this->userDefinedSimplefield[] = $userDefinedSimplefield;
        return $this;
    }

    /**
     * isset userDefinedSimplefield
     *
     * @param int|string $index
     * @return bool
     */
    public function issetUserDefinedSimplefield($index)
    {
        return isset($this->userDefinedSimplefield[$index]);
    }

    /**
     * unset userDefinedSimplefield
     *
     * @param int|string $index
     * @return void
     */
    public function unsetUserDefinedSimplefield($index)
    {
        unset($this->userDefinedSimplefield[$index]);
    }

    /**
     * Gets as userDefinedSimplefield
     *
     * @return \Zwei14\OpenImmo\API\UserDefinedSimplefield[]
     */
    public function getUserDefinedSimplefield()
    {
        return $this->userDefinedSimplefield;
    }

    /**
     * Sets a new userDefinedSimplefield
     *
     * @param \Zwei14\OpenImmo\API\UserDefinedSimplefield[] $userDefinedSimplefield
     * @return self
     */
    public function setUserDefinedSimplefield(array $userDefinedSimplefield)
    {
        $this->userDefinedSimplefield = $userDefinedSimplefield;
        return $this;
    }

    /**
     * Adds as userDefinedAnyfield
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\UserDefinedAnyfield $userDefinedAnyfield
     */
    public function addToUserDefinedAnyfield(\Zwei14\OpenImmo\API\UserDefinedAnyfield $userDefinedAnyfield)
    {
        $this->userDefinedAnyfield[] = $userDefinedAnyfield;
        return $this;
    }

    /**
     * isset userDefinedAnyfield
     *
     * @param int|string $index
     * @return bool
     */
    public function issetUserDefinedAnyfield($index)
    {
        return isset($this->userDefinedAnyfield[$index]);
    }

    /**
     * unset userDefinedAnyfield
     *
     * @param int|string $index
     * @return void
     */
    public function unsetUserDefinedAnyfield($index)
    {
        unset($this->userDefinedAnyfield[$index]);
    }

    /**
     * Gets as userDefinedAnyfield
     *
     * @return \Zwei14\OpenImmo\API\UserDefinedAnyfield[]
     */
    public function getUserDefinedAnyfield()
    {
        return $this->userDefinedAnyfield;
    }

    /**
     * Sets a new userDefinedAnyfield
     *
     * @param \Zwei14\OpenImmo\API\UserDefinedAnyfield[] $userDefinedAnyfield
     * @return self
     */
    public function setUserDefinedAnyfield(array $userDefinedAnyfield)
    {
        $this->userDefinedAnyfield = $userDefinedAnyfield;
        return $this;
    }

    /**
     * Adds as feld
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType $feld
     */
    public function addToUserDefinedExtend(\Zwei14\OpenImmo\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType $feld)
    {
        $this->userDefinedExtend[] = $feld;
        return $this;
    }

    /**
     * isset userDefinedExtend
     *
     * @param int|string $index
     * @return bool
     */
    public function issetUserDefinedExtend($index)
    {
        return isset($this->userDefinedExtend[$index]);
    }

    /**
     * unset userDefinedExtend
     *
     * @param int|string $index
     * @return void
     */
    public function unsetUserDefinedExtend($index)
    {
        unset($this->userDefinedExtend[$index]);
    }

    /**
     * Gets as userDefinedExtend
     *
     * @return \Zwei14\OpenImmo\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[]
     */
    public function getUserDefinedExtend()
    {
        return $this->userDefinedExtend;
    }

    /**
     * Sets a new userDefinedExtend
     *
     * @param \Zwei14\OpenImmo\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[] $userDefinedExtend
     * @return self
     */
    public function setUserDefinedExtend(array $userDefinedExtend)
    {
        $this->userDefinedExtend = $userDefinedExtend;
        return $this;
    }


}

