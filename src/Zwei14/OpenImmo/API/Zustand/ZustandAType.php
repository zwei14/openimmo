<?php

namespace Zwei14\OpenImmo\API\Zustand;

/**
 * Class representing ZustandAType
 */
class ZustandAType
{

    /**
     * @var string $zustandArt
     */
    private $zustandArt = null;

    /**
     * Gets as zustandArt
     *
     * @return string
     */
    public function getZustandArt()
    {
        return $this->zustandArt;
    }

    /**
     * Sets a new zustandArt
     *
     * @param string $zustandArt
     * @return self
     */
    public function setZustandArt($zustandArt)
    {
        $this->zustandArt = $zustandArt;
        return $this;
    }


}

