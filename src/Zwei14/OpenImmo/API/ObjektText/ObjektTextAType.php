<?php

namespace Zwei14\OpenImmo\API\ObjektText;

/**
 * Class representing ObjektTextAType
 */
class ObjektTextAType
{

    /**
     * @var string $lang
     */
    private $lang = null;

    /**
     * Gets as lang
     *
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Sets a new lang
     *
     * @param string $lang
     * @return self
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
        return $this;
    }


}

