<?php

namespace Zwei14\OpenImmo\API\ZinshausRenditeobjekt;

/**
 * Class representing ZinshausRenditeobjektAType
 */
class ZinshausRenditeobjektAType
{

    /**
     * @var string $zinsTyp
     */
    private $zinsTyp = null;

    /**
     * Gets as zinsTyp
     *
     * @return string
     */
    public function getZinsTyp()
    {
        return $this->zinsTyp;
    }

    /**
     * Sets a new zinsTyp
     *
     * @param string $zinsTyp
     * @return self
     */
    public function setZinsTyp($zinsTyp)
    {
        $this->zinsTyp = $zinsTyp;
        return $this;
    }


}

