<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\Zustand\ZustandAType;

/**
 * Class representing Zustand
 *
 * Zustand des Objektes, Optionen nicht kombinierbar
 */
class Zustand extends ZustandAType
{


}

