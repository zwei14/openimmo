<?php

namespace Zwei14\OpenImmo\API\Parken;

/**
 * Class representing ParkenAType
 */
class ParkenAType
{

    /**
     * @var string $parkenTyp
     */
    private $parkenTyp = null;

    /**
     * Gets as parkenTyp
     *
     * @return string
     */
    public function getParkenTyp()
    {
        return $this->parkenTyp;
    }

    /**
     * Sets a new parkenTyp
     *
     * @param string $parkenTyp
     * @return self
     */
    public function setParkenTyp($parkenTyp)
    {
        $this->parkenTyp = $parkenTyp;
        return $this;
    }


}

