<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\Kaufpreisnetto\KaufpreisnettoAType;

/**
 * Class representing Kaufpreisnetto
 *
 * Ausgewiesene Kaufpreis Netto, Optional mit Umst im Attribut. Speziell für Gewerbe
 */
class Kaufpreisnetto extends KaufpreisnettoAType
{


}

