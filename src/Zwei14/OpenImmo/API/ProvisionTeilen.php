<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\ProvisionTeilen\ProvisionTeilenAType;

/**
 * Class representing ProvisionTeilen
 *
 * Aufteilen der provision bei Partnergeschäften. Auch "A Meta" Geschäft. Attribut zeigt, wie der Wert angegeben wird: fester wert, prozent, oder Text Information
 */
class ProvisionTeilen extends ProvisionTeilenAType
{


}

