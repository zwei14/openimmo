<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\MieteinnahmenIst\MieteinnahmenIstAType;

/**
 * Class representing MieteinnahmenIst
 *
 * Mieteinnahmen pro Periode, Momentan-/Isteinnahmen (Ohne Periode = JAHR)
 */
class MieteinnahmenIst extends MieteinnahmenIstAType
{


}

