<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\MieteinnahmenSoll\MieteinnahmenSollAType;

/**
 * Class representing MieteinnahmenSoll
 *
 * Mieteinnahmen pro Periode, Normal-/Solleinnahmen (Ohne Periode = JAHR)
 */
class MieteinnahmenSoll extends MieteinnahmenSollAType
{


}

