<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\Verkaufstatus\VerkaufstatusAType;

/**
 * Class representing Verkaufstatus
 *
 * Anzeige ob z.B schon verkauft, Optionen nicht kombinierbar
 */
class Verkaufstatus extends VerkaufstatusAType
{


}

