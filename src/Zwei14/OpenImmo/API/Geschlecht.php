<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\Geschlecht\GeschlechtAType;

/**
 * Class representing Geschlecht
 *
 * Soll das Objekt nur an Frauen bzw. nur an Männer vermietet werden, 
 *  fehlende Angabe wird als 'Ja' interpretiert
 */
class Geschlecht extends GeschlechtAType
{


}

