<?php

namespace Zwei14\OpenImmo\API\Anhang;

/**
 * Class representing AnhangAType
 */
class AnhangAType
{

    /**
     * @var string $location
     */
    private $location = null;

    /**
     * @var string $gruppe
     */
    private $gruppe = null;

    /**
     * @var string $anhangtitel
     */
    private $anhangtitel = null;

    /**
     * @var string $format
     */
    private $format = null;

    /**
     * @var \Zwei14\OpenImmo\API\Check $check
     */
    private $check = null;

    /**
     * @var \Zwei14\OpenImmo\API\Daten $daten
     */
    private $daten = null;

    /**
     * Gets as location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Sets a new location
     *
     * @param string $location
     * @return self
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * Gets as gruppe
     *
     * @return string
     */
    public function getGruppe()
    {
        return $this->gruppe;
    }

    /**
     * Sets a new gruppe
     *
     * @param string $gruppe
     * @return self
     */
    public function setGruppe($gruppe)
    {
        $this->gruppe = $gruppe;
        return $this;
    }

    /**
     * Gets as anhangtitel
     *
     * @return string
     */
    public function getAnhangtitel()
    {
        return $this->anhangtitel;
    }

    /**
     * Sets a new anhangtitel
     *
     * @param string $anhangtitel
     * @return self
     */
    public function setAnhangtitel($anhangtitel)
    {
        $this->anhangtitel = $anhangtitel;
        return $this;
    }

    /**
     * Gets as format
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Sets a new format
     *
     * @param string $format
     * @return self
     */
    public function setFormat($format)
    {
        $this->format = $format;
        return $this;
    }

    /**
     * Gets as check
     *
     * @return \Zwei14\OpenImmo\API\Check
     */
    public function getCheck()
    {
        return $this->check;
    }

    /**
     * Sets a new check
     *
     * @param \Zwei14\OpenImmo\API\Check $check
     * @return self
     */
    public function setCheck(\Zwei14\OpenImmo\API\Check $check)
    {
        $this->check = $check;
        return $this;
    }

    /**
     * Gets as daten
     *
     * @return \Zwei14\OpenImmo\API\Daten
     */
    public function getDaten()
    {
        return $this->daten;
    }

    /**
     * Sets a new daten
     *
     * @param \Zwei14\OpenImmo\API\Daten $daten
     * @return self
     */
    public function setDaten(\Zwei14\OpenImmo\API\Daten $daten)
    {
        $this->daten = $daten;
        return $this;
    }


}

