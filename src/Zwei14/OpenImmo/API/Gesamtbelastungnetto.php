<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\Gesamtbelastungnetto\GesamtbelastungnettoAType;

/**
 * Class representing Gesamtbelastungnetto
 *
 * Die Summe alle Nebenkosten und Mietzinse bei Miete, UmSt. im Attribut.
 */
class Gesamtbelastungnetto extends GesamtbelastungnettoAType
{


}

