<?php

namespace Zwei14\OpenImmo\API\HallenLagerProd;

/**
 * Class representing HallenLagerProdAType
 */
class HallenLagerProdAType
{

    /**
     * @var string $hallenTyp
     */
    private $hallenTyp = null;

    /**
     * Gets as hallenTyp
     *
     * @return string
     */
    public function getHallenTyp()
    {
        return $this->hallenTyp;
    }

    /**
     * Sets a new hallenTyp
     *
     * @param string $hallenTyp
     * @return self
     */
    public function setHallenTyp($hallenTyp)
    {
        $this->hallenTyp = $hallenTyp;
        return $this;
    }


}

