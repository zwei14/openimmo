<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\AusrichtBalkonTerrasse\AusrichtBalkonTerrasseAType;

/**
 * Class representing AusrichtBalkonTerrasse
 *
 * Ausrichtung der Balkone bzw. der Terrassen, Optionen kombinierbar
 */
class AusrichtBalkonTerrasse extends AusrichtBalkonTerrasseAType
{


}

