<?php

namespace Zwei14\OpenImmo\API\Moebliert;

/**
 * Class representing MoebliertAType
 */
class MoebliertAType
{

    /**
     * @var string $moeb
     */
    private $moeb = null;

    /**
     * Gets as moeb
     *
     * @return string
     */
    public function getMoeb()
    {
        return $this->moeb;
    }

    /**
     * Sets a new moeb
     *
     * @param string $moeb
     * @return self
     */
    public function setMoeb($moeb)
    {
        $this->moeb = $moeb;
        return $this;
    }


}

