<?php

namespace Zwei14\OpenImmo\API\Infrastruktur;

/**
 * Class representing InfrastrukturAType
 */
class InfrastrukturAType
{

    /**
     * @var bool $zulieferung
     */
    private $zulieferung = null;

    /**
     * @var \Zwei14\OpenImmo\API\Ausblick $ausblick
     */
    private $ausblick = null;

    /**
     * @var \Zwei14\OpenImmo\API\Distanzen[] $distanzen
     */
    private $distanzen = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\DistanzenSport[] $distanzenSport
     */
    private $distanzenSport = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\UserDefinedSimplefield[] $userDefinedSimplefield
     */
    private $userDefinedSimplefield = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\UserDefinedAnyfield[] $userDefinedAnyfield
     */
    private $userDefinedAnyfield = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[] $userDefinedExtend
     */
    private $userDefinedExtend = null;

    /**
     * Gets as zulieferung
     *
     * @return bool
     */
    public function getZulieferung()
    {
        return $this->zulieferung;
    }

    /**
     * Sets a new zulieferung
     *
     * @param bool $zulieferung
     * @return self
     */
    public function setZulieferung($zulieferung)
    {
        $this->zulieferung = $zulieferung;
        return $this;
    }

    /**
     * Gets as ausblick
     *
     * @return \Zwei14\OpenImmo\API\Ausblick
     */
    public function getAusblick()
    {
        return $this->ausblick;
    }

    /**
     * Sets a new ausblick
     *
     * @param \Zwei14\OpenImmo\API\Ausblick $ausblick
     * @return self
     */
    public function setAusblick(\Zwei14\OpenImmo\API\Ausblick $ausblick)
    {
        $this->ausblick = $ausblick;
        return $this;
    }

    /**
     * Adds as distanzen
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\Distanzen $distanzen
     */
    public function addToDistanzen(\Zwei14\OpenImmo\API\Distanzen $distanzen)
    {
        $this->distanzen[] = $distanzen;
        return $this;
    }

    /**
     * isset distanzen
     *
     * @param int|string $index
     * @return bool
     */
    public function issetDistanzen($index)
    {
        return isset($this->distanzen[$index]);
    }

    /**
     * unset distanzen
     *
     * @param int|string $index
     * @return void
     */
    public function unsetDistanzen($index)
    {
        unset($this->distanzen[$index]);
    }

    /**
     * Gets as distanzen
     *
     * @return \Zwei14\OpenImmo\API\Distanzen[]
     */
    public function getDistanzen()
    {
        return $this->distanzen;
    }

    /**
     * Sets a new distanzen
     *
     * @param \Zwei14\OpenImmo\API\Distanzen[] $distanzen
     * @return self
     */
    public function setDistanzen(array $distanzen)
    {
        $this->distanzen = $distanzen;
        return $this;
    }

    /**
     * Adds as distanzenSport
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\DistanzenSport $distanzenSport
     */
    public function addToDistanzenSport(\Zwei14\OpenImmo\API\DistanzenSport $distanzenSport)
    {
        $this->distanzenSport[] = $distanzenSport;
        return $this;
    }

    /**
     * isset distanzenSport
     *
     * @param int|string $index
     * @return bool
     */
    public function issetDistanzenSport($index)
    {
        return isset($this->distanzenSport[$index]);
    }

    /**
     * unset distanzenSport
     *
     * @param int|string $index
     * @return void
     */
    public function unsetDistanzenSport($index)
    {
        unset($this->distanzenSport[$index]);
    }

    /**
     * Gets as distanzenSport
     *
     * @return \Zwei14\OpenImmo\API\DistanzenSport[]
     */
    public function getDistanzenSport()
    {
        return $this->distanzenSport;
    }

    /**
     * Sets a new distanzenSport
     *
     * @param \Zwei14\OpenImmo\API\DistanzenSport[] $distanzenSport
     * @return self
     */
    public function setDistanzenSport(array $distanzenSport)
    {
        $this->distanzenSport = $distanzenSport;
        return $this;
    }

    /**
     * Adds as userDefinedSimplefield
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\UserDefinedSimplefield $userDefinedSimplefield
     */
    public function addToUserDefinedSimplefield(\Zwei14\OpenImmo\API\UserDefinedSimplefield $userDefinedSimplefield)
    {
        $this->userDefinedSimplefield[] = $userDefinedSimplefield;
        return $this;
    }

    /**
     * isset userDefinedSimplefield
     *
     * @param int|string $index
     * @return bool
     */
    public function issetUserDefinedSimplefield($index)
    {
        return isset($this->userDefinedSimplefield[$index]);
    }

    /**
     * unset userDefinedSimplefield
     *
     * @param int|string $index
     * @return void
     */
    public function unsetUserDefinedSimplefield($index)
    {
        unset($this->userDefinedSimplefield[$index]);
    }

    /**
     * Gets as userDefinedSimplefield
     *
     * @return \Zwei14\OpenImmo\API\UserDefinedSimplefield[]
     */
    public function getUserDefinedSimplefield()
    {
        return $this->userDefinedSimplefield;
    }

    /**
     * Sets a new userDefinedSimplefield
     *
     * @param \Zwei14\OpenImmo\API\UserDefinedSimplefield[] $userDefinedSimplefield
     * @return self
     */
    public function setUserDefinedSimplefield(array $userDefinedSimplefield)
    {
        $this->userDefinedSimplefield = $userDefinedSimplefield;
        return $this;
    }

    /**
     * Adds as userDefinedAnyfield
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\UserDefinedAnyfield $userDefinedAnyfield
     */
    public function addToUserDefinedAnyfield(\Zwei14\OpenImmo\API\UserDefinedAnyfield $userDefinedAnyfield)
    {
        $this->userDefinedAnyfield[] = $userDefinedAnyfield;
        return $this;
    }

    /**
     * isset userDefinedAnyfield
     *
     * @param int|string $index
     * @return bool
     */
    public function issetUserDefinedAnyfield($index)
    {
        return isset($this->userDefinedAnyfield[$index]);
    }

    /**
     * unset userDefinedAnyfield
     *
     * @param int|string $index
     * @return void
     */
    public function unsetUserDefinedAnyfield($index)
    {
        unset($this->userDefinedAnyfield[$index]);
    }

    /**
     * Gets as userDefinedAnyfield
     *
     * @return \Zwei14\OpenImmo\API\UserDefinedAnyfield[]
     */
    public function getUserDefinedAnyfield()
    {
        return $this->userDefinedAnyfield;
    }

    /**
     * Sets a new userDefinedAnyfield
     *
     * @param \Zwei14\OpenImmo\API\UserDefinedAnyfield[] $userDefinedAnyfield
     * @return self
     */
    public function setUserDefinedAnyfield(array $userDefinedAnyfield)
    {
        $this->userDefinedAnyfield = $userDefinedAnyfield;
        return $this;
    }

    /**
     * Adds as feld
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType $feld
     */
    public function addToUserDefinedExtend(\Zwei14\OpenImmo\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType $feld)
    {
        $this->userDefinedExtend[] = $feld;
        return $this;
    }

    /**
     * isset userDefinedExtend
     *
     * @param int|string $index
     * @return bool
     */
    public function issetUserDefinedExtend($index)
    {
        return isset($this->userDefinedExtend[$index]);
    }

    /**
     * unset userDefinedExtend
     *
     * @param int|string $index
     * @return void
     */
    public function unsetUserDefinedExtend($index)
    {
        unset($this->userDefinedExtend[$index]);
    }

    /**
     * Gets as userDefinedExtend
     *
     * @return \Zwei14\OpenImmo\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[]
     */
    public function getUserDefinedExtend()
    {
        return $this->userDefinedExtend;
    }

    /**
     * Sets a new userDefinedExtend
     *
     * @param \Zwei14\OpenImmo\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[] $userDefinedExtend
     * @return self
     */
    public function setUserDefinedExtend(array $userDefinedExtend)
    {
        $this->userDefinedExtend = $userDefinedExtend;
        return $this;
    }


}

