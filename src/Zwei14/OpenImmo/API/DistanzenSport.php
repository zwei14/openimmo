<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\DistanzenSport\DistanzenSportAType;

/**
 * Class representing DistanzenSport
 *
 * Welche Distanz zu dem ausgewählen Sport-/Freizeitziel besteht(Angabe in km), 
 *  Optionen nicht kombinierbar, Distanzelement ist mehrfach erfassbar
 */
class DistanzenSport extends DistanzenSportAType
{


}

