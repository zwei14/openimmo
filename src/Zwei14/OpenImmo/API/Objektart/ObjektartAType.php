<?php

namespace Zwei14\OpenImmo\API\Objektart;

/**
 * Class representing ObjektartAType
 */
class ObjektartAType
{

    /**
     * @var \Zwei14\OpenImmo\API\Zimmer[] $zimmer
     */
    private $zimmer = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\Wohnung[] $wohnung
     */
    private $wohnung = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\Haus[] $haus
     */
    private $haus = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\Grundstueck[] $grundstueck
     */
    private $grundstueck = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\BueroPraxen[] $bueroPraxen
     */
    private $bueroPraxen = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\Einzelhandel[] $einzelhandel
     */
    private $einzelhandel = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\Gastgewerbe[] $gastgewerbe
     */
    private $gastgewerbe = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\HallenLagerProd[] $hallenLagerProd
     */
    private $hallenLagerProd = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\LandUndForstwirtschaft[] $landUndForstwirtschaft
     */
    private $landUndForstwirtschaft = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\Parken[] $parken
     */
    private $parken = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\Sonstige[] $sonstige
     */
    private $sonstige = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\FreizeitimmobilieGewerblich[] $freizeitimmobilieGewerblich
     */
    private $freizeitimmobilieGewerblich = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\ZinshausRenditeobjekt[] $zinshausRenditeobjekt
     */
    private $zinshausRenditeobjekt = [
        
    ];

    /**
     * @var string[] $objektartZusatz
     */
    private $objektartZusatz = [
        
    ];

    /**
     * Adds as zimmer
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\Zimmer $zimmer
     */
    public function addToZimmer(\Zwei14\OpenImmo\API\Zimmer $zimmer)
    {
        $this->zimmer[] = $zimmer;
        return $this;
    }

    /**
     * isset zimmer
     *
     * @param int|string $index
     * @return bool
     */
    public function issetZimmer($index)
    {
        return isset($this->zimmer[$index]);
    }

    /**
     * unset zimmer
     *
     * @param int|string $index
     * @return void
     */
    public function unsetZimmer($index)
    {
        unset($this->zimmer[$index]);
    }

    /**
     * Gets as zimmer
     *
     * @return \Zwei14\OpenImmo\API\Zimmer[]
     */
    public function getZimmer()
    {
        return $this->zimmer;
    }

    /**
     * Sets a new zimmer
     *
     * @param \Zwei14\OpenImmo\API\Zimmer[] $zimmer
     * @return self
     */
    public function setZimmer(array $zimmer)
    {
        $this->zimmer = $zimmer;
        return $this;
    }

    /**
     * Adds as wohnung
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\Wohnung $wohnung
     */
    public function addToWohnung(\Zwei14\OpenImmo\API\Wohnung $wohnung)
    {
        $this->wohnung[] = $wohnung;
        return $this;
    }

    /**
     * isset wohnung
     *
     * @param int|string $index
     * @return bool
     */
    public function issetWohnung($index)
    {
        return isset($this->wohnung[$index]);
    }

    /**
     * unset wohnung
     *
     * @param int|string $index
     * @return void
     */
    public function unsetWohnung($index)
    {
        unset($this->wohnung[$index]);
    }

    /**
     * Gets as wohnung
     *
     * @return \Zwei14\OpenImmo\API\Wohnung[]
     */
    public function getWohnung()
    {
        return $this->wohnung;
    }

    /**
     * Sets a new wohnung
     *
     * @param \Zwei14\OpenImmo\API\Wohnung[] $wohnung
     * @return self
     */
    public function setWohnung(array $wohnung)
    {
        $this->wohnung = $wohnung;
        return $this;
    }

    /**
     * Adds as haus
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\Haus $haus
     */
    public function addToHaus(\Zwei14\OpenImmo\API\Haus $haus)
    {
        $this->haus[] = $haus;
        return $this;
    }

    /**
     * isset haus
     *
     * @param int|string $index
     * @return bool
     */
    public function issetHaus($index)
    {
        return isset($this->haus[$index]);
    }

    /**
     * unset haus
     *
     * @param int|string $index
     * @return void
     */
    public function unsetHaus($index)
    {
        unset($this->haus[$index]);
    }

    /**
     * Gets as haus
     *
     * @return \Zwei14\OpenImmo\API\Haus[]
     */
    public function getHaus()
    {
        return $this->haus;
    }

    /**
     * Sets a new haus
     *
     * @param \Zwei14\OpenImmo\API\Haus[] $haus
     * @return self
     */
    public function setHaus(array $haus)
    {
        $this->haus = $haus;
        return $this;
    }

    /**
     * Adds as grundstueck
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\Grundstueck $grundstueck
     */
    public function addToGrundstueck(\Zwei14\OpenImmo\API\Grundstueck $grundstueck)
    {
        $this->grundstueck[] = $grundstueck;
        return $this;
    }

    /**
     * isset grundstueck
     *
     * @param int|string $index
     * @return bool
     */
    public function issetGrundstueck($index)
    {
        return isset($this->grundstueck[$index]);
    }

    /**
     * unset grundstueck
     *
     * @param int|string $index
     * @return void
     */
    public function unsetGrundstueck($index)
    {
        unset($this->grundstueck[$index]);
    }

    /**
     * Gets as grundstueck
     *
     * @return \Zwei14\OpenImmo\API\Grundstueck[]
     */
    public function getGrundstueck()
    {
        return $this->grundstueck;
    }

    /**
     * Sets a new grundstueck
     *
     * @param \Zwei14\OpenImmo\API\Grundstueck[] $grundstueck
     * @return self
     */
    public function setGrundstueck(array $grundstueck)
    {
        $this->grundstueck = $grundstueck;
        return $this;
    }

    /**
     * Adds as bueroPraxen
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\BueroPraxen $bueroPraxen
     */
    public function addToBueroPraxen(\Zwei14\OpenImmo\API\BueroPraxen $bueroPraxen)
    {
        $this->bueroPraxen[] = $bueroPraxen;
        return $this;
    }

    /**
     * isset bueroPraxen
     *
     * @param int|string $index
     * @return bool
     */
    public function issetBueroPraxen($index)
    {
        return isset($this->bueroPraxen[$index]);
    }

    /**
     * unset bueroPraxen
     *
     * @param int|string $index
     * @return void
     */
    public function unsetBueroPraxen($index)
    {
        unset($this->bueroPraxen[$index]);
    }

    /**
     * Gets as bueroPraxen
     *
     * @return \Zwei14\OpenImmo\API\BueroPraxen[]
     */
    public function getBueroPraxen()
    {
        return $this->bueroPraxen;
    }

    /**
     * Sets a new bueroPraxen
     *
     * @param \Zwei14\OpenImmo\API\BueroPraxen[] $bueroPraxen
     * @return self
     */
    public function setBueroPraxen(array $bueroPraxen)
    {
        $this->bueroPraxen = $bueroPraxen;
        return $this;
    }

    /**
     * Adds as einzelhandel
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\Einzelhandel $einzelhandel
     */
    public function addToEinzelhandel(\Zwei14\OpenImmo\API\Einzelhandel $einzelhandel)
    {
        $this->einzelhandel[] = $einzelhandel;
        return $this;
    }

    /**
     * isset einzelhandel
     *
     * @param int|string $index
     * @return bool
     */
    public function issetEinzelhandel($index)
    {
        return isset($this->einzelhandel[$index]);
    }

    /**
     * unset einzelhandel
     *
     * @param int|string $index
     * @return void
     */
    public function unsetEinzelhandel($index)
    {
        unset($this->einzelhandel[$index]);
    }

    /**
     * Gets as einzelhandel
     *
     * @return \Zwei14\OpenImmo\API\Einzelhandel[]
     */
    public function getEinzelhandel()
    {
        return $this->einzelhandel;
    }

    /**
     * Sets a new einzelhandel
     *
     * @param \Zwei14\OpenImmo\API\Einzelhandel[] $einzelhandel
     * @return self
     */
    public function setEinzelhandel(array $einzelhandel)
    {
        $this->einzelhandel = $einzelhandel;
        return $this;
    }

    /**
     * Adds as gastgewerbe
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\Gastgewerbe $gastgewerbe
     */
    public function addToGastgewerbe(\Zwei14\OpenImmo\API\Gastgewerbe $gastgewerbe)
    {
        $this->gastgewerbe[] = $gastgewerbe;
        return $this;
    }

    /**
     * isset gastgewerbe
     *
     * @param int|string $index
     * @return bool
     */
    public function issetGastgewerbe($index)
    {
        return isset($this->gastgewerbe[$index]);
    }

    /**
     * unset gastgewerbe
     *
     * @param int|string $index
     * @return void
     */
    public function unsetGastgewerbe($index)
    {
        unset($this->gastgewerbe[$index]);
    }

    /**
     * Gets as gastgewerbe
     *
     * @return \Zwei14\OpenImmo\API\Gastgewerbe[]
     */
    public function getGastgewerbe()
    {
        return $this->gastgewerbe;
    }

    /**
     * Sets a new gastgewerbe
     *
     * @param \Zwei14\OpenImmo\API\Gastgewerbe[] $gastgewerbe
     * @return self
     */
    public function setGastgewerbe(array $gastgewerbe)
    {
        $this->gastgewerbe = $gastgewerbe;
        return $this;
    }

    /**
     * Adds as hallenLagerProd
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\HallenLagerProd $hallenLagerProd
     */
    public function addToHallenLagerProd(\Zwei14\OpenImmo\API\HallenLagerProd $hallenLagerProd)
    {
        $this->hallenLagerProd[] = $hallenLagerProd;
        return $this;
    }

    /**
     * isset hallenLagerProd
     *
     * @param int|string $index
     * @return bool
     */
    public function issetHallenLagerProd($index)
    {
        return isset($this->hallenLagerProd[$index]);
    }

    /**
     * unset hallenLagerProd
     *
     * @param int|string $index
     * @return void
     */
    public function unsetHallenLagerProd($index)
    {
        unset($this->hallenLagerProd[$index]);
    }

    /**
     * Gets as hallenLagerProd
     *
     * @return \Zwei14\OpenImmo\API\HallenLagerProd[]
     */
    public function getHallenLagerProd()
    {
        return $this->hallenLagerProd;
    }

    /**
     * Sets a new hallenLagerProd
     *
     * @param \Zwei14\OpenImmo\API\HallenLagerProd[] $hallenLagerProd
     * @return self
     */
    public function setHallenLagerProd(array $hallenLagerProd)
    {
        $this->hallenLagerProd = $hallenLagerProd;
        return $this;
    }

    /**
     * Adds as landUndForstwirtschaft
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\LandUndForstwirtschaft $landUndForstwirtschaft
     */
    public function addToLandUndForstwirtschaft(\Zwei14\OpenImmo\API\LandUndForstwirtschaft $landUndForstwirtschaft)
    {
        $this->landUndForstwirtschaft[] = $landUndForstwirtschaft;
        return $this;
    }

    /**
     * isset landUndForstwirtschaft
     *
     * @param int|string $index
     * @return bool
     */
    public function issetLandUndForstwirtschaft($index)
    {
        return isset($this->landUndForstwirtschaft[$index]);
    }

    /**
     * unset landUndForstwirtschaft
     *
     * @param int|string $index
     * @return void
     */
    public function unsetLandUndForstwirtschaft($index)
    {
        unset($this->landUndForstwirtschaft[$index]);
    }

    /**
     * Gets as landUndForstwirtschaft
     *
     * @return \Zwei14\OpenImmo\API\LandUndForstwirtschaft[]
     */
    public function getLandUndForstwirtschaft()
    {
        return $this->landUndForstwirtschaft;
    }

    /**
     * Sets a new landUndForstwirtschaft
     *
     * @param \Zwei14\OpenImmo\API\LandUndForstwirtschaft[] $landUndForstwirtschaft
     * @return self
     */
    public function setLandUndForstwirtschaft(array $landUndForstwirtschaft)
    {
        $this->landUndForstwirtschaft = $landUndForstwirtschaft;
        return $this;
    }

    /**
     * Adds as parken
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\Parken $parken
     */
    public function addToParken(\Zwei14\OpenImmo\API\Parken $parken)
    {
        $this->parken[] = $parken;
        return $this;
    }

    /**
     * isset parken
     *
     * @param int|string $index
     * @return bool
     */
    public function issetParken($index)
    {
        return isset($this->parken[$index]);
    }

    /**
     * unset parken
     *
     * @param int|string $index
     * @return void
     */
    public function unsetParken($index)
    {
        unset($this->parken[$index]);
    }

    /**
     * Gets as parken
     *
     * @return \Zwei14\OpenImmo\API\Parken[]
     */
    public function getParken()
    {
        return $this->parken;
    }

    /**
     * Sets a new parken
     *
     * @param \Zwei14\OpenImmo\API\Parken[] $parken
     * @return self
     */
    public function setParken(array $parken)
    {
        $this->parken = $parken;
        return $this;
    }

    /**
     * Adds as sonstige
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\Sonstige $sonstige
     */
    public function addToSonstige(\Zwei14\OpenImmo\API\Sonstige $sonstige)
    {
        $this->sonstige[] = $sonstige;
        return $this;
    }

    /**
     * isset sonstige
     *
     * @param int|string $index
     * @return bool
     */
    public function issetSonstige($index)
    {
        return isset($this->sonstige[$index]);
    }

    /**
     * unset sonstige
     *
     * @param int|string $index
     * @return void
     */
    public function unsetSonstige($index)
    {
        unset($this->sonstige[$index]);
    }

    /**
     * Gets as sonstige
     *
     * @return \Zwei14\OpenImmo\API\Sonstige[]
     */
    public function getSonstige()
    {
        return $this->sonstige;
    }

    /**
     * Sets a new sonstige
     *
     * @param \Zwei14\OpenImmo\API\Sonstige[] $sonstige
     * @return self
     */
    public function setSonstige(array $sonstige)
    {
        $this->sonstige = $sonstige;
        return $this;
    }

    /**
     * Adds as freizeitimmobilieGewerblich
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\FreizeitimmobilieGewerblich $freizeitimmobilieGewerblich
     */
    public function addToFreizeitimmobilieGewerblich(\Zwei14\OpenImmo\API\FreizeitimmobilieGewerblich $freizeitimmobilieGewerblich)
    {
        $this->freizeitimmobilieGewerblich[] = $freizeitimmobilieGewerblich;
        return $this;
    }

    /**
     * isset freizeitimmobilieGewerblich
     *
     * @param int|string $index
     * @return bool
     */
    public function issetFreizeitimmobilieGewerblich($index)
    {
        return isset($this->freizeitimmobilieGewerblich[$index]);
    }

    /**
     * unset freizeitimmobilieGewerblich
     *
     * @param int|string $index
     * @return void
     */
    public function unsetFreizeitimmobilieGewerblich($index)
    {
        unset($this->freizeitimmobilieGewerblich[$index]);
    }

    /**
     * Gets as freizeitimmobilieGewerblich
     *
     * @return \Zwei14\OpenImmo\API\FreizeitimmobilieGewerblich[]
     */
    public function getFreizeitimmobilieGewerblich()
    {
        return $this->freizeitimmobilieGewerblich;
    }

    /**
     * Sets a new freizeitimmobilieGewerblich
     *
     * @param \Zwei14\OpenImmo\API\FreizeitimmobilieGewerblich[] $freizeitimmobilieGewerblich
     * @return self
     */
    public function setFreizeitimmobilieGewerblich(array $freizeitimmobilieGewerblich)
    {
        $this->freizeitimmobilieGewerblich = $freizeitimmobilieGewerblich;
        return $this;
    }

    /**
     * Adds as zinshausRenditeobjekt
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\ZinshausRenditeobjekt $zinshausRenditeobjekt
     */
    public function addToZinshausRenditeobjekt(\Zwei14\OpenImmo\API\ZinshausRenditeobjekt $zinshausRenditeobjekt)
    {
        $this->zinshausRenditeobjekt[] = $zinshausRenditeobjekt;
        return $this;
    }

    /**
     * isset zinshausRenditeobjekt
     *
     * @param int|string $index
     * @return bool
     */
    public function issetZinshausRenditeobjekt($index)
    {
        return isset($this->zinshausRenditeobjekt[$index]);
    }

    /**
     * unset zinshausRenditeobjekt
     *
     * @param int|string $index
     * @return void
     */
    public function unsetZinshausRenditeobjekt($index)
    {
        unset($this->zinshausRenditeobjekt[$index]);
    }

    /**
     * Gets as zinshausRenditeobjekt
     *
     * @return \Zwei14\OpenImmo\API\ZinshausRenditeobjekt[]
     */
    public function getZinshausRenditeobjekt()
    {
        return $this->zinshausRenditeobjekt;
    }

    /**
     * Sets a new zinshausRenditeobjekt
     *
     * @param \Zwei14\OpenImmo\API\ZinshausRenditeobjekt[] $zinshausRenditeobjekt
     * @return self
     */
    public function setZinshausRenditeobjekt(array $zinshausRenditeobjekt)
    {
        $this->zinshausRenditeobjekt = $zinshausRenditeobjekt;
        return $this;
    }

    /**
     * Adds as objektartZusatz
     *
     * @return self
     * @param string $objektartZusatz
     */
    public function addToObjektartZusatz($objektartZusatz)
    {
        $this->objektartZusatz[] = $objektartZusatz;
        return $this;
    }

    /**
     * isset objektartZusatz
     *
     * @param int|string $index
     * @return bool
     */
    public function issetObjektartZusatz($index)
    {
        return isset($this->objektartZusatz[$index]);
    }

    /**
     * unset objektartZusatz
     *
     * @param int|string $index
     * @return void
     */
    public function unsetObjektartZusatz($index)
    {
        unset($this->objektartZusatz[$index]);
    }

    /**
     * Gets as objektartZusatz
     *
     * @return string[]
     */
    public function getObjektartZusatz()
    {
        return $this->objektartZusatz;
    }

    /**
     * Sets a new objektartZusatz
     *
     * @param string[] $objektartZusatz
     * @return self
     */
    public function setObjektartZusatz(array $objektartZusatz)
    {
        $this->objektartZusatz = $objektartZusatz;
        return $this;
    }


}

