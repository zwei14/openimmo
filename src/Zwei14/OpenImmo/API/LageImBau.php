<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\LageImBau\LageImBauAType;

/**
 * Class representing LageImBau
 *
 * Angabe über die Lage der Immobilie im Gesamtgebäude, Optionen kombinierbar
 */
class LageImBau extends LageImBauAType
{


}

