<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\Geokoordinaten\GeokoordinatenAType;

/**
 * Class representing Geokoordinaten
 *
 * Geokoordinaten der Immobilie, Pflichtfeld, alternativ mit Ort, PLZ
 */
class Geokoordinaten extends GeokoordinatenAType
{


}

