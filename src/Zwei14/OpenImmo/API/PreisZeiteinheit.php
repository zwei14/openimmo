<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\PreisZeiteinheit\PreisZeiteinheitAType;

/**
 * Class representing PreisZeiteinheit
 *
 * Zeiteinheit für die der Preis gilt, vorrangig bei Ferienobjekten
 */
class PreisZeiteinheit extends PreisZeiteinheitAType
{


}

