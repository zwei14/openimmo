<?php

namespace Zwei14\OpenImmo\API\Immobilie;

/**
 * Class representing ImmobilieAType
 */
class ImmobilieAType
{

    /**
     * @var \Zwei14\OpenImmo\API\Objektkategorie $objektkategorie
     */
    private $objektkategorie = null;

    /**
     * @var \Zwei14\OpenImmo\API\Geo $geo
     */
    private $geo = null;

    /**
     * @var \Zwei14\OpenImmo\API\Kontaktperson $kontaktperson
     */
    private $kontaktperson = null;

    /**
     * @var \Zwei14\OpenImmo\API\WeitereAdresse[] $weitereAdresse
     */
    private $weitereAdresse = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\Preise $preise
     */
    private $preise = null;

    /**
     * @var \Zwei14\OpenImmo\API\Bieterverfahren $bieterverfahren
     */
    private $bieterverfahren = null;

    /**
     * @var \Zwei14\OpenImmo\API\Versteigerung $versteigerung
     */
    private $versteigerung = null;

    /**
     * @var \Zwei14\OpenImmo\API\Flaechen $flaechen
     */
    private $flaechen = null;

    /**
     * @var \Zwei14\OpenImmo\API\Ausstattung $ausstattung
     */
    private $ausstattung = null;

    /**
     * @var \Zwei14\OpenImmo\API\ZustandAngaben $zustandAngaben
     */
    private $zustandAngaben = null;

    /**
     * @var \Zwei14\OpenImmo\API\Bewertung\BewertungAType\FeldAType[] $bewertung
     */
    private $bewertung = null;

    /**
     * @var \Zwei14\OpenImmo\API\Infrastruktur $infrastruktur
     */
    private $infrastruktur = null;

    /**
     * @var \Zwei14\OpenImmo\API\Freitexte $freitexte
     */
    private $freitexte = null;

    /**
     * @var \Zwei14\OpenImmo\API\Anhaenge $anhaenge
     */
    private $anhaenge = null;

    /**
     * @var \Zwei14\OpenImmo\API\VerwaltungObjekt $verwaltungObjekt
     */
    private $verwaltungObjekt = null;

    /**
     * @var \Zwei14\OpenImmo\API\VerwaltungTechn $verwaltungTechn
     */
    private $verwaltungTechn = null;

    /**
     * @var \Zwei14\OpenImmo\API\UserDefinedSimplefield[] $userDefinedSimplefield
     */
    private $userDefinedSimplefield = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\UserDefinedAnyfield[] $userDefinedAnyfield
     */
    private $userDefinedAnyfield = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[] $userDefinedExtend
     */
    private $userDefinedExtend = null;

    /**
     * Gets as objektkategorie
     *
     * @return \Zwei14\OpenImmo\API\Objektkategorie
     */
    public function getObjektkategorie()
    {
        return $this->objektkategorie;
    }

    /**
     * Sets a new objektkategorie
     *
     * @param \Zwei14\OpenImmo\API\Objektkategorie $objektkategorie
     * @return self
     */
    public function setObjektkategorie(\Zwei14\OpenImmo\API\Objektkategorie $objektkategorie)
    {
        $this->objektkategorie = $objektkategorie;
        return $this;
    }

    /**
     * Gets as geo
     *
     * @return \Zwei14\OpenImmo\API\Geo
     */
    public function getGeo()
    {
        return $this->geo;
    }

    /**
     * Sets a new geo
     *
     * @param \Zwei14\OpenImmo\API\Geo $geo
     * @return self
     */
    public function setGeo(\Zwei14\OpenImmo\API\Geo $geo)
    {
        $this->geo = $geo;
        return $this;
    }

    /**
     * Gets as kontaktperson
     *
     * @return \Zwei14\OpenImmo\API\Kontaktperson
     */
    public function getKontaktperson()
    {
        return $this->kontaktperson;
    }

    /**
     * Sets a new kontaktperson
     *
     * @param \Zwei14\OpenImmo\API\Kontaktperson $kontaktperson
     * @return self
     */
    public function setKontaktperson(\Zwei14\OpenImmo\API\Kontaktperson $kontaktperson)
    {
        $this->kontaktperson = $kontaktperson;
        return $this;
    }

    /**
     * Adds as weitereAdresse
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\WeitereAdresse $weitereAdresse
     */
    public function addToWeitereAdresse(\Zwei14\OpenImmo\API\WeitereAdresse $weitereAdresse)
    {
        $this->weitereAdresse[] = $weitereAdresse;
        return $this;
    }

    /**
     * isset weitereAdresse
     *
     * @param int|string $index
     * @return bool
     */
    public function issetWeitereAdresse($index)
    {
        return isset($this->weitereAdresse[$index]);
    }

    /**
     * unset weitereAdresse
     *
     * @param int|string $index
     * @return void
     */
    public function unsetWeitereAdresse($index)
    {
        unset($this->weitereAdresse[$index]);
    }

    /**
     * Gets as weitereAdresse
     *
     * @return \Zwei14\OpenImmo\API\WeitereAdresse[]
     */
    public function getWeitereAdresse()
    {
        return $this->weitereAdresse;
    }

    /**
     * Sets a new weitereAdresse
     *
     * @param \Zwei14\OpenImmo\API\WeitereAdresse[] $weitereAdresse
     * @return self
     */
    public function setWeitereAdresse(array $weitereAdresse)
    {
        $this->weitereAdresse = $weitereAdresse;
        return $this;
    }

    /**
     * Gets as preise
     *
     * @return \Zwei14\OpenImmo\API\Preise
     */
    public function getPreise()
    {
        return $this->preise;
    }

    /**
     * Sets a new preise
     *
     * @param \Zwei14\OpenImmo\API\Preise $preise
     * @return self
     */
    public function setPreise(\Zwei14\OpenImmo\API\Preise $preise)
    {
        $this->preise = $preise;
        return $this;
    }

    /**
     * Gets as bieterverfahren
     *
     * @return \Zwei14\OpenImmo\API\Bieterverfahren
     */
    public function getBieterverfahren()
    {
        return $this->bieterverfahren;
    }

    /**
     * Sets a new bieterverfahren
     *
     * @param \Zwei14\OpenImmo\API\Bieterverfahren $bieterverfahren
     * @return self
     */
    public function setBieterverfahren(\Zwei14\OpenImmo\API\Bieterverfahren $bieterverfahren)
    {
        $this->bieterverfahren = $bieterverfahren;
        return $this;
    }

    /**
     * Gets as versteigerung
     *
     * @return \Zwei14\OpenImmo\API\Versteigerung
     */
    public function getVersteigerung()
    {
        return $this->versteigerung;
    }

    /**
     * Sets a new versteigerung
     *
     * @param \Zwei14\OpenImmo\API\Versteigerung $versteigerung
     * @return self
     */
    public function setVersteigerung(\Zwei14\OpenImmo\API\Versteigerung $versteigerung)
    {
        $this->versteigerung = $versteigerung;
        return $this;
    }

    /**
     * Gets as flaechen
     *
     * @return \Zwei14\OpenImmo\API\Flaechen
     */
    public function getFlaechen()
    {
        return $this->flaechen;
    }

    /**
     * Sets a new flaechen
     *
     * @param \Zwei14\OpenImmo\API\Flaechen $flaechen
     * @return self
     */
    public function setFlaechen(\Zwei14\OpenImmo\API\Flaechen $flaechen)
    {
        $this->flaechen = $flaechen;
        return $this;
    }

    /**
     * Gets as ausstattung
     *
     * @return \Zwei14\OpenImmo\API\Ausstattung
     */
    public function getAusstattung()
    {
        return $this->ausstattung;
    }

    /**
     * Sets a new ausstattung
     *
     * @param \Zwei14\OpenImmo\API\Ausstattung $ausstattung
     * @return self
     */
    public function setAusstattung(\Zwei14\OpenImmo\API\Ausstattung $ausstattung)
    {
        $this->ausstattung = $ausstattung;
        return $this;
    }

    /**
     * Gets as zustandAngaben
     *
     * @return \Zwei14\OpenImmo\API\ZustandAngaben
     */
    public function getZustandAngaben()
    {
        return $this->zustandAngaben;
    }

    /**
     * Sets a new zustandAngaben
     *
     * @param \Zwei14\OpenImmo\API\ZustandAngaben $zustandAngaben
     * @return self
     */
    public function setZustandAngaben(\Zwei14\OpenImmo\API\ZustandAngaben $zustandAngaben)
    {
        $this->zustandAngaben = $zustandAngaben;
        return $this;
    }

    /**
     * Adds as feld
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\Bewertung\BewertungAType\FeldAType $feld
     */
    public function addToBewertung(\Zwei14\OpenImmo\API\Bewertung\BewertungAType\FeldAType $feld)
    {
        $this->bewertung[] = $feld;
        return $this;
    }

    /**
     * isset bewertung
     *
     * @param int|string $index
     * @return bool
     */
    public function issetBewertung($index)
    {
        return isset($this->bewertung[$index]);
    }

    /**
     * unset bewertung
     *
     * @param int|string $index
     * @return void
     */
    public function unsetBewertung($index)
    {
        unset($this->bewertung[$index]);
    }

    /**
     * Gets as bewertung
     *
     * @return \Zwei14\OpenImmo\API\Bewertung\BewertungAType\FeldAType[]
     */
    public function getBewertung()
    {
        return $this->bewertung;
    }

    /**
     * Sets a new bewertung
     *
     * @param \Zwei14\OpenImmo\API\Bewertung\BewertungAType\FeldAType[] $bewertung
     * @return self
     */
    public function setBewertung(array $bewertung)
    {
        $this->bewertung = $bewertung;
        return $this;
    }

    /**
     * Gets as infrastruktur
     *
     * @return \Zwei14\OpenImmo\API\Infrastruktur
     */
    public function getInfrastruktur()
    {
        return $this->infrastruktur;
    }

    /**
     * Sets a new infrastruktur
     *
     * @param \Zwei14\OpenImmo\API\Infrastruktur $infrastruktur
     * @return self
     */
    public function setInfrastruktur(\Zwei14\OpenImmo\API\Infrastruktur $infrastruktur)
    {
        $this->infrastruktur = $infrastruktur;
        return $this;
    }

    /**
     * Gets as freitexte
     *
     * @return \Zwei14\OpenImmo\API\Freitexte
     */
    public function getFreitexte()
    {
        return $this->freitexte;
    }

    /**
     * Sets a new freitexte
     *
     * @param \Zwei14\OpenImmo\API\Freitexte $freitexte
     * @return self
     */
    public function setFreitexte(\Zwei14\OpenImmo\API\Freitexte $freitexte)
    {
        $this->freitexte = $freitexte;
        return $this;
    }

    /**
     * Gets as anhaenge
     *
     * @return \Zwei14\OpenImmo\API\Anhaenge
     */
    public function getAnhaenge()
    {
        return $this->anhaenge;
    }

    /**
     * Sets a new anhaenge
     *
     * @param \Zwei14\OpenImmo\API\Anhaenge $anhaenge
     * @return self
     */
    public function setAnhaenge(\Zwei14\OpenImmo\API\Anhaenge $anhaenge)
    {
        $this->anhaenge = $anhaenge;
        return $this;
    }

    /**
     * Gets as verwaltungObjekt
     *
     * @return \Zwei14\OpenImmo\API\VerwaltungObjekt
     */
    public function getVerwaltungObjekt()
    {
        return $this->verwaltungObjekt;
    }

    /**
     * Sets a new verwaltungObjekt
     *
     * @param \Zwei14\OpenImmo\API\VerwaltungObjekt $verwaltungObjekt
     * @return self
     */
    public function setVerwaltungObjekt(\Zwei14\OpenImmo\API\VerwaltungObjekt $verwaltungObjekt)
    {
        $this->verwaltungObjekt = $verwaltungObjekt;
        return $this;
    }

    /**
     * Gets as verwaltungTechn
     *
     * @return \Zwei14\OpenImmo\API\VerwaltungTechn
     */
    public function getVerwaltungTechn()
    {
        return $this->verwaltungTechn;
    }

    /**
     * Sets a new verwaltungTechn
     *
     * @param \Zwei14\OpenImmo\API\VerwaltungTechn $verwaltungTechn
     * @return self
     */
    public function setVerwaltungTechn(\Zwei14\OpenImmo\API\VerwaltungTechn $verwaltungTechn)
    {
        $this->verwaltungTechn = $verwaltungTechn;
        return $this;
    }

    /**
     * Adds as userDefinedSimplefield
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\UserDefinedSimplefield $userDefinedSimplefield
     */
    public function addToUserDefinedSimplefield(\Zwei14\OpenImmo\API\UserDefinedSimplefield $userDefinedSimplefield)
    {
        $this->userDefinedSimplefield[] = $userDefinedSimplefield;
        return $this;
    }

    /**
     * isset userDefinedSimplefield
     *
     * @param int|string $index
     * @return bool
     */
    public function issetUserDefinedSimplefield($index)
    {
        return isset($this->userDefinedSimplefield[$index]);
    }

    /**
     * unset userDefinedSimplefield
     *
     * @param int|string $index
     * @return void
     */
    public function unsetUserDefinedSimplefield($index)
    {
        unset($this->userDefinedSimplefield[$index]);
    }

    /**
     * Gets as userDefinedSimplefield
     *
     * @return \Zwei14\OpenImmo\API\UserDefinedSimplefield[]
     */
    public function getUserDefinedSimplefield()
    {
        return $this->userDefinedSimplefield;
    }

    /**
     * Sets a new userDefinedSimplefield
     *
     * @param \Zwei14\OpenImmo\API\UserDefinedSimplefield[] $userDefinedSimplefield
     * @return self
     */
    public function setUserDefinedSimplefield(array $userDefinedSimplefield)
    {
        $this->userDefinedSimplefield = $userDefinedSimplefield;
        return $this;
    }

    /**
     * Adds as userDefinedAnyfield
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\UserDefinedAnyfield $userDefinedAnyfield
     */
    public function addToUserDefinedAnyfield(\Zwei14\OpenImmo\API\UserDefinedAnyfield $userDefinedAnyfield)
    {
        $this->userDefinedAnyfield[] = $userDefinedAnyfield;
        return $this;
    }

    /**
     * isset userDefinedAnyfield
     *
     * @param int|string $index
     * @return bool
     */
    public function issetUserDefinedAnyfield($index)
    {
        return isset($this->userDefinedAnyfield[$index]);
    }

    /**
     * unset userDefinedAnyfield
     *
     * @param int|string $index
     * @return void
     */
    public function unsetUserDefinedAnyfield($index)
    {
        unset($this->userDefinedAnyfield[$index]);
    }

    /**
     * Gets as userDefinedAnyfield
     *
     * @return \Zwei14\OpenImmo\API\UserDefinedAnyfield[]
     */
    public function getUserDefinedAnyfield()
    {
        return $this->userDefinedAnyfield;
    }

    /**
     * Sets a new userDefinedAnyfield
     *
     * @param \Zwei14\OpenImmo\API\UserDefinedAnyfield[] $userDefinedAnyfield
     * @return self
     */
    public function setUserDefinedAnyfield(array $userDefinedAnyfield)
    {
        $this->userDefinedAnyfield = $userDefinedAnyfield;
        return $this;
    }

    /**
     * Adds as feld
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType $feld
     */
    public function addToUserDefinedExtend(\Zwei14\OpenImmo\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType $feld)
    {
        $this->userDefinedExtend[] = $feld;
        return $this;
    }

    /**
     * isset userDefinedExtend
     *
     * @param int|string $index
     * @return bool
     */
    public function issetUserDefinedExtend($index)
    {
        return isset($this->userDefinedExtend[$index]);
    }

    /**
     * unset userDefinedExtend
     *
     * @param int|string $index
     * @return void
     */
    public function unsetUserDefinedExtend($index)
    {
        unset($this->userDefinedExtend[$index]);
    }

    /**
     * Gets as userDefinedExtend
     *
     * @return \Zwei14\OpenImmo\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[]
     */
    public function getUserDefinedExtend()
    {
        return $this->userDefinedExtend;
    }

    /**
     * Sets a new userDefinedExtend
     *
     * @param \Zwei14\OpenImmo\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[] $userDefinedExtend
     * @return self
     */
    public function setUserDefinedExtend(array $userDefinedExtend)
    {
        $this->userDefinedExtend = $userDefinedExtend;
        return $this;
    }


}

