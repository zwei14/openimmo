<?php

namespace Zwei14\OpenImmo\API\MaxMietdauer;

/**
 * Class representing MaxMietdauerAType
 */
class MaxMietdauerAType
{

    /**
     * @var string $maxDauer
     */
    private $maxDauer = null;

    /**
     * Gets as maxDauer
     *
     * @return string
     */
    public function getMaxDauer()
    {
        return $this->maxDauer;
    }

    /**
     * Sets a new maxDauer
     *
     * @param string $maxDauer
     * @return self
     */
    public function setMaxDauer($maxDauer)
    {
        $this->maxDauer = $maxDauer;
        return $this;
    }


}

