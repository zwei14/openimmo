<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\Distanzen\DistanzenAType;

/**
 * Class representing Distanzen
 *
 * Welche Distanz zu dem ausgewählten Ziel besteht (Angabe in km), 
 *  Optionen nicht kombinierbar, Distanzelement ist mehrfach erfassbar
 */
class Distanzen extends DistanzenAType
{


}

