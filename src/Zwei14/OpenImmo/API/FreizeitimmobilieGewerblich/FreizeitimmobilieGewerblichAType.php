<?php

namespace Zwei14\OpenImmo\API\FreizeitimmobilieGewerblich;

/**
 * Class representing FreizeitimmobilieGewerblichAType
 */
class FreizeitimmobilieGewerblichAType
{

    /**
     * @var string $freizeitTyp
     */
    private $freizeitTyp = null;

    /**
     * Gets as freizeitTyp
     *
     * @return string
     */
    public function getFreizeitTyp()
    {
        return $this->freizeitTyp;
    }

    /**
     * Sets a new freizeitTyp
     *
     * @param string $freizeitTyp
     * @return self
     */
    public function setFreizeitTyp($freizeitTyp)
    {
        $this->freizeitTyp = $freizeitTyp;
        return $this;
    }


}

