<?php

namespace Zwei14\OpenImmo\API\Verkaufstatus;

/**
 * Class representing VerkaufstatusAType
 */
class VerkaufstatusAType
{

    /**
     * @var string $stand
     */
    private $stand = null;

    /**
     * Gets as stand
     *
     * @return string
     */
    public function getStand()
    {
        return $this->stand;
    }

    /**
     * Sets a new stand
     *
     * @param string $stand
     * @return self
     */
    public function setStand($stand)
    {
        $this->stand = $stand;
        return $this;
    }


}

