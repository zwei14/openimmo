<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\MaxMietdauer\MaxMietdauerAType;

/**
 * Class representing MaxMietdauer
 *
 * Maximalzeitraum für den die Immobilie gemietet werdenkann, Optionen nicht kombinierbar, vorrangig bei WaZ
 */
class MaxMietdauer extends MaxMietdauerAType
{


}

