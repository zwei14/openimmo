<?php

namespace Zwei14\OpenImmo\API\Aktion;

/**
 * Class representing AktionAType
 */
class AktionAType
{

    /**
     * @var string $aktionart
     */
    private $aktionart = null;

    /**
     * Gets as aktionart
     *
     * @return string
     */
    public function getAktionart()
    {
        return $this->aktionart;
    }

    /**
     * Sets a new aktionart
     *
     * @param string $aktionart
     * @return self
     */
    public function setAktionart($aktionart)
    {
        $this->aktionart = $aktionart;
        return $this;
    }


}

