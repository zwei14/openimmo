<?php

namespace Zwei14\OpenImmo\API\Gastgewerbe;

/**
 * Class representing GastgewerbeAType
 */
class GastgewerbeAType
{

    /**
     * @var string $gastgewTyp
     */
    private $gastgewTyp = null;

    /**
     * Gets as gastgewTyp
     *
     * @return string
     */
    public function getGastgewTyp()
    {
        return $this->gastgewTyp;
    }

    /**
     * Sets a new gastgewTyp
     *
     * @param string $gastgewTyp
     * @return self
     */
    public function setGastgewTyp($gastgewTyp)
    {
        $this->gastgewTyp = $gastgewTyp;
        return $this;
    }


}

