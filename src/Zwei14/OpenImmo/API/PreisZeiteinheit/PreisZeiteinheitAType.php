<?php

namespace Zwei14\OpenImmo\API\PreisZeiteinheit;

/**
 * Class representing PreisZeiteinheitAType
 */
class PreisZeiteinheitAType
{

    /**
     * @var string $zeiteinheit
     */
    private $zeiteinheit = null;

    /**
     * Gets as zeiteinheit
     *
     * @return string
     */
    public function getZeiteinheit()
    {
        return $this->zeiteinheit;
    }

    /**
     * Sets a new zeiteinheit
     *
     * @param string $zeiteinheit
     * @return self
     */
    public function setZeiteinheit($zeiteinheit)
    {
        $this->zeiteinheit = $zeiteinheit;
        return $this;
    }


}

