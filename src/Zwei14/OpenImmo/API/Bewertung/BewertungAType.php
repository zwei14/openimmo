<?php

namespace Zwei14\OpenImmo\API\Bewertung;

/**
 * Class representing BewertungAType
 */
class BewertungAType
{

    /**
     * @var \Zwei14\OpenImmo\API\Bewertung\BewertungAType\FeldAType[] $feld
     */
    private $feld = [
        
    ];

    /**
     * Adds as feld
     *
     * @return self
     * @param \Zwei14\OpenImmo\API\Bewertung\BewertungAType\FeldAType $feld
     */
    public function addToFeld(\Zwei14\OpenImmo\API\Bewertung\BewertungAType\FeldAType $feld)
    {
        $this->feld[] = $feld;
        return $this;
    }

    /**
     * isset feld
     *
     * @param int|string $index
     * @return bool
     */
    public function issetFeld($index)
    {
        return isset($this->feld[$index]);
    }

    /**
     * unset feld
     *
     * @param int|string $index
     * @return void
     */
    public function unsetFeld($index)
    {
        unset($this->feld[$index]);
    }

    /**
     * Gets as feld
     *
     * @return \Zwei14\OpenImmo\API\Bewertung\BewertungAType\FeldAType[]
     */
    public function getFeld()
    {
        return $this->feld;
    }

    /**
     * Sets a new feld
     *
     * @param \Zwei14\OpenImmo\API\Bewertung\BewertungAType\FeldAType[] $feld
     * @return self
     */
    public function setFeld(array $feld)
    {
        $this->feld = $feld;
        return $this;
    }


}

