<?php

namespace Zwei14\OpenImmo\API;

use Zwei14\OpenImmo\API\Sonstige\SonstigeAType;

/**
 * Class representing Sonstige
 *
 * Objektart / Typ f. Sonstiges
 * Bitte ab Version 1.2.3 die Attribute GARAGEN, PARKFLACHE nicht mehr verwenden. 
 * Objekte befinden sich jetzt unter Element parken.
 * Aus kompatibilitätegründen bleiben die Attribute NOCH! erhalten.
 * In nachfolgenden Versionen wird die Unterstützung an dieser Stelle eingestellt.
 */
class Sonstige extends SonstigeAType
{


}

