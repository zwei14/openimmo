<?php

namespace Zwei14\OpenImmo\Feedback\API\Interessent;

/**
 * Class representing InteressentAType
 */
class InteressentAType
{

    /**
     * @var string $intId
     */
    private $intId = null;

    /**
     * @var string $anrede
     */
    private $anrede = null;

    /**
     * @var string $vorname
     */
    private $vorname = null;

    /**
     * @var string $nachname
     */
    private $nachname = null;

    /**
     * @var string $firma
     */
    private $firma = null;

    /**
     * @var string $strasse
     */
    private $strasse = null;

    /**
     * @var string $postfach
     */
    private $postfach = null;

    /**
     * @var string $plz
     */
    private $plz = null;

    /**
     * @var string $ort
     */
    private $ort = null;

    /**
     * @var string $tel
     */
    private $tel = null;

    /**
     * @var string $fax
     */
    private $fax = null;

    /**
     * @var string $mobil
     */
    private $mobil = null;

    /**
     * @var string $email
     */
    private $email = null;

    /**
     * Bevorzugte Kontaktart des Kunden. Attribut kann mehrfach vorkommen
     *
     * @var string[] $bevorzugt
     */
    private $bevorzugt = [
        
    ];

    /**
     * Der Info-/Aktivitätenwunsch des Kunden. Attribut kann mehrfach vorkommen
     *
     * @var string[] $wunsch
     */
    private $wunsch = [
        
    ];

    /**
     * @var string $anfrage
     */
    private $anfrage = null;

    /**
     * @var \Zwei14\OpenImmo\Feedback\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[] $userDefinedExtend
     */
    private $userDefinedExtend = null;

    /**
     * Gets as intId
     *
     * @return string
     */
    public function getIntId()
    {
        return $this->intId;
    }

    /**
     * Sets a new intId
     *
     * @param string $intId
     * @return self
     */
    public function setIntId($intId)
    {
        $this->intId = $intId;
        return $this;
    }

    /**
     * Gets as anrede
     *
     * @return string
     */
    public function getAnrede()
    {
        return $this->anrede;
    }

    /**
     * Sets a new anrede
     *
     * @param string $anrede
     * @return self
     */
    public function setAnrede($anrede)
    {
        $this->anrede = $anrede;
        return $this;
    }

    /**
     * Gets as vorname
     *
     * @return string
     */
    public function getVorname()
    {
        return $this->vorname;
    }

    /**
     * Sets a new vorname
     *
     * @param string $vorname
     * @return self
     */
    public function setVorname($vorname)
    {
        $this->vorname = $vorname;
        return $this;
    }

    /**
     * Gets as nachname
     *
     * @return string
     */
    public function getNachname()
    {
        return $this->nachname;
    }

    /**
     * Sets a new nachname
     *
     * @param string $nachname
     * @return self
     */
    public function setNachname($nachname)
    {
        $this->nachname = $nachname;
        return $this;
    }

    /**
     * Gets as firma
     *
     * @return string
     */
    public function getFirma()
    {
        return $this->firma;
    }

    /**
     * Sets a new firma
     *
     * @param string $firma
     * @return self
     */
    public function setFirma($firma)
    {
        $this->firma = $firma;
        return $this;
    }

    /**
     * Gets as strasse
     *
     * @return string
     */
    public function getStrasse()
    {
        return $this->strasse;
    }

    /**
     * Sets a new strasse
     *
     * @param string $strasse
     * @return self
     */
    public function setStrasse($strasse)
    {
        $this->strasse = $strasse;
        return $this;
    }

    /**
     * Gets as postfach
     *
     * @return string
     */
    public function getPostfach()
    {
        return $this->postfach;
    }

    /**
     * Sets a new postfach
     *
     * @param string $postfach
     * @return self
     */
    public function setPostfach($postfach)
    {
        $this->postfach = $postfach;
        return $this;
    }

    /**
     * Gets as plz
     *
     * @return string
     */
    public function getPlz()
    {
        return $this->plz;
    }

    /**
     * Sets a new plz
     *
     * @param string $plz
     * @return self
     */
    public function setPlz($plz)
    {
        $this->plz = $plz;
        return $this;
    }

    /**
     * Gets as ort
     *
     * @return string
     */
    public function getOrt()
    {
        return $this->ort;
    }

    /**
     * Sets a new ort
     *
     * @param string $ort
     * @return self
     */
    public function setOrt($ort)
    {
        $this->ort = $ort;
        return $this;
    }

    /**
     * Gets as tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Sets a new tel
     *
     * @param string $tel
     * @return self
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
        return $this;
    }

    /**
     * Gets as fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Sets a new fax
     *
     * @param string $fax
     * @return self
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
        return $this;
    }

    /**
     * Gets as mobil
     *
     * @return string
     */
    public function getMobil()
    {
        return $this->mobil;
    }

    /**
     * Sets a new mobil
     *
     * @param string $mobil
     * @return self
     */
    public function setMobil($mobil)
    {
        $this->mobil = $mobil;
        return $this;
    }

    /**
     * Gets as email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets a new email
     *
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Adds as bevorzugt
     *
     * Bevorzugte Kontaktart des Kunden. Attribut kann mehrfach vorkommen
     *
     * @return self
     * @param string $bevorzugt
     */
    public function addToBevorzugt($bevorzugt)
    {
        $this->bevorzugt[] = $bevorzugt;
        return $this;
    }

    /**
     * isset bevorzugt
     *
     * Bevorzugte Kontaktart des Kunden. Attribut kann mehrfach vorkommen
     *
     * @param int|string $index
     * @return bool
     */
    public function issetBevorzugt($index)
    {
        return isset($this->bevorzugt[$index]);
    }

    /**
     * unset bevorzugt
     *
     * Bevorzugte Kontaktart des Kunden. Attribut kann mehrfach vorkommen
     *
     * @param int|string $index
     * @return void
     */
    public function unsetBevorzugt($index)
    {
        unset($this->bevorzugt[$index]);
    }

    /**
     * Gets as bevorzugt
     *
     * Bevorzugte Kontaktart des Kunden. Attribut kann mehrfach vorkommen
     *
     * @return string[]
     */
    public function getBevorzugt()
    {
        return $this->bevorzugt;
    }

    /**
     * Sets a new bevorzugt
     *
     * Bevorzugte Kontaktart des Kunden. Attribut kann mehrfach vorkommen
     *
     * @param string $bevorzugt
     * @return self
     */
    public function setBevorzugt(array $bevorzugt)
    {
        $this->bevorzugt = $bevorzugt;
        return $this;
    }

    /**
     * Adds as wunsch
     *
     * Der Info-/Aktivitätenwunsch des Kunden. Attribut kann mehrfach vorkommen
     *
     * @return self
     * @param string $wunsch
     */
    public function addToWunsch($wunsch)
    {
        $this->wunsch[] = $wunsch;
        return $this;
    }

    /**
     * isset wunsch
     *
     * Der Info-/Aktivitätenwunsch des Kunden. Attribut kann mehrfach vorkommen
     *
     * @param int|string $index
     * @return bool
     */
    public function issetWunsch($index)
    {
        return isset($this->wunsch[$index]);
    }

    /**
     * unset wunsch
     *
     * Der Info-/Aktivitätenwunsch des Kunden. Attribut kann mehrfach vorkommen
     *
     * @param int|string $index
     * @return void
     */
    public function unsetWunsch($index)
    {
        unset($this->wunsch[$index]);
    }

    /**
     * Gets as wunsch
     *
     * Der Info-/Aktivitätenwunsch des Kunden. Attribut kann mehrfach vorkommen
     *
     * @return string[]
     */
    public function getWunsch()
    {
        return $this->wunsch;
    }

    /**
     * Sets a new wunsch
     *
     * Der Info-/Aktivitätenwunsch des Kunden. Attribut kann mehrfach vorkommen
     *
     * @param string $wunsch
     * @return self
     */
    public function setWunsch(array $wunsch)
    {
        $this->wunsch = $wunsch;
        return $this;
    }

    /**
     * Gets as anfrage
     *
     * @return string
     */
    public function getAnfrage()
    {
        return $this->anfrage;
    }

    /**
     * Sets a new anfrage
     *
     * @param string $anfrage
     * @return self
     */
    public function setAnfrage($anfrage)
    {
        $this->anfrage = $anfrage;
        return $this;
    }

    /**
     * Adds as feld
     *
     * @return self
     * @param \Zwei14\OpenImmo\Feedback\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType $feld
     */
    public function addToUserDefinedExtend(\Zwei14\OpenImmo\Feedback\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType $feld)
    {
        $this->userDefinedExtend[] = $feld;
        return $this;
    }

    /**
     * isset userDefinedExtend
     *
     * @param int|string $index
     * @return bool
     */
    public function issetUserDefinedExtend($index)
    {
        return isset($this->userDefinedExtend[$index]);
    }

    /**
     * unset userDefinedExtend
     *
     * @param int|string $index
     * @return void
     */
    public function unsetUserDefinedExtend($index)
    {
        unset($this->userDefinedExtend[$index]);
    }

    /**
     * Gets as userDefinedExtend
     *
     * @return \Zwei14\OpenImmo\Feedback\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[]
     */
    public function getUserDefinedExtend()
    {
        return $this->userDefinedExtend;
    }

    /**
     * Sets a new userDefinedExtend
     *
     * @param \Zwei14\OpenImmo\Feedback\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[] $userDefinedExtend
     * @return self
     */
    public function setUserDefinedExtend(array $userDefinedExtend)
    {
        $this->userDefinedExtend = $userDefinedExtend;
        return $this;
    }


}

