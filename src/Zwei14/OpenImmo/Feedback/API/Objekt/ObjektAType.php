<?php

namespace Zwei14\OpenImmo\Feedback\API\Objekt;

/**
 * Class representing ObjektAType
 */
class ObjektAType
{

    /**
     * @var string $portalUniqueId
     */
    private $portalUniqueId = null;

    /**
     * @var string $portalObjId
     */
    private $portalObjId = null;

    /**
     * @var string $anbieterId
     */
    private $anbieterId = null;

    /**
     * @var string $oobjId
     */
    private $oobjId = null;

    /**
     * @var string $zusatzRefnr
     */
    private $zusatzRefnr = null;

    /**
     * @var string $exposeUrl
     */
    private $exposeUrl = null;

    /**
     * @var string[] $vermarktungsart
     */
    private $vermarktungsart = [
        
    ];

    /**
     * @var string $bezeichnung
     */
    private $bezeichnung = null;

    /**
     * @var string $etage
     */
    private $etage = null;

    /**
     * @var string $whgNr
     */
    private $whgNr = null;

    /**
     * @var string $strasse
     */
    private $strasse = null;

    /**
     * @var string $ort
     */
    private $ort = null;

    /**
     * @var string $land
     */
    private $land = null;

    /**
     * @var string $stadtbezirk
     */
    private $stadtbezirk = null;

    /**
     * @var string $preis
     */
    private $preis = null;

    /**
     * @var string $gebot
     */
    private $gebot = null;

    /**
     * @var string $wae
     */
    private $wae = null;

    /**
     * @var string $anzahlZimmer
     */
    private $anzahlZimmer = null;

    /**
     * @var string $flaeche
     */
    private $flaeche = null;

    /**
     * @var \Zwei14\OpenImmo\Feedback\API\Interessent[] $interessent
     */
    private $interessent = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\Feedback\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[] $userDefinedExtend
     */
    private $userDefinedExtend = null;

    /**
     * Gets as portalUniqueId
     *
     * @return string
     */
    public function getPortalUniqueId()
    {
        return $this->portalUniqueId;
    }

    /**
     * Sets a new portalUniqueId
     *
     * @param string $portalUniqueId
     * @return self
     */
    public function setPortalUniqueId($portalUniqueId)
    {
        $this->portalUniqueId = $portalUniqueId;
        return $this;
    }

    /**
     * Gets as portalObjId
     *
     * @return string
     */
    public function getPortalObjId()
    {
        return $this->portalObjId;
    }

    /**
     * Sets a new portalObjId
     *
     * @param string $portalObjId
     * @return self
     */
    public function setPortalObjId($portalObjId)
    {
        $this->portalObjId = $portalObjId;
        return $this;
    }

    /**
     * Gets as anbieterId
     *
     * @return string
     */
    public function getAnbieterId()
    {
        return $this->anbieterId;
    }

    /**
     * Sets a new anbieterId
     *
     * @param string $anbieterId
     * @return self
     */
    public function setAnbieterId($anbieterId)
    {
        $this->anbieterId = $anbieterId;
        return $this;
    }

    /**
     * Gets as oobjId
     *
     * @return string
     */
    public function getOobjId()
    {
        return $this->oobjId;
    }

    /**
     * Sets a new oobjId
     *
     * @param string $oobjId
     * @return self
     */
    public function setOobjId($oobjId)
    {
        $this->oobjId = $oobjId;
        return $this;
    }

    /**
     * Gets as zusatzRefnr
     *
     * @return string
     */
    public function getZusatzRefnr()
    {
        return $this->zusatzRefnr;
    }

    /**
     * Sets a new zusatzRefnr
     *
     * @param string $zusatzRefnr
     * @return self
     */
    public function setZusatzRefnr($zusatzRefnr)
    {
        $this->zusatzRefnr = $zusatzRefnr;
        return $this;
    }

    /**
     * Gets as exposeUrl
     *
     * @return string
     */
    public function getExposeUrl()
    {
        return $this->exposeUrl;
    }

    /**
     * Sets a new exposeUrl
     *
     * @param string $exposeUrl
     * @return self
     */
    public function setExposeUrl($exposeUrl)
    {
        $this->exposeUrl = $exposeUrl;
        return $this;
    }

    /**
     * Adds as vermarktungsart
     *
     * @return self
     * @param string $vermarktungsart
     */
    public function addToVermarktungsart($vermarktungsart)
    {
        $this->vermarktungsart[] = $vermarktungsart;
        return $this;
    }

    /**
     * isset vermarktungsart
     *
     * @param int|string $index
     * @return bool
     */
    public function issetVermarktungsart($index)
    {
        return isset($this->vermarktungsart[$index]);
    }

    /**
     * unset vermarktungsart
     *
     * @param int|string $index
     * @return void
     */
    public function unsetVermarktungsart($index)
    {
        unset($this->vermarktungsart[$index]);
    }

    /**
     * Gets as vermarktungsart
     *
     * @return string[]
     */
    public function getVermarktungsart()
    {
        return $this->vermarktungsart;
    }

    /**
     * Sets a new vermarktungsart
     *
     * @param string[] $vermarktungsart
     * @return self
     */
    public function setVermarktungsart(array $vermarktungsart)
    {
        $this->vermarktungsart = $vermarktungsart;
        return $this;
    }

    /**
     * Gets as bezeichnung
     *
     * @return string
     */
    public function getBezeichnung()
    {
        return $this->bezeichnung;
    }

    /**
     * Sets a new bezeichnung
     *
     * @param string $bezeichnung
     * @return self
     */
    public function setBezeichnung($bezeichnung)
    {
        $this->bezeichnung = $bezeichnung;
        return $this;
    }

    /**
     * Gets as etage
     *
     * @return string
     */
    public function getEtage()
    {
        return $this->etage;
    }

    /**
     * Sets a new etage
     *
     * @param string $etage
     * @return self
     */
    public function setEtage($etage)
    {
        $this->etage = $etage;
        return $this;
    }

    /**
     * Gets as whgNr
     *
     * @return string
     */
    public function getWhgNr()
    {
        return $this->whgNr;
    }

    /**
     * Sets a new whgNr
     *
     * @param string $whgNr
     * @return self
     */
    public function setWhgNr($whgNr)
    {
        $this->whgNr = $whgNr;
        return $this;
    }

    /**
     * Gets as strasse
     *
     * @return string
     */
    public function getStrasse()
    {
        return $this->strasse;
    }

    /**
     * Sets a new strasse
     *
     * @param string $strasse
     * @return self
     */
    public function setStrasse($strasse)
    {
        $this->strasse = $strasse;
        return $this;
    }

    /**
     * Gets as ort
     *
     * @return string
     */
    public function getOrt()
    {
        return $this->ort;
    }

    /**
     * Sets a new ort
     *
     * @param string $ort
     * @return self
     */
    public function setOrt($ort)
    {
        $this->ort = $ort;
        return $this;
    }

    /**
     * Gets as land
     *
     * @return string
     */
    public function getLand()
    {
        return $this->land;
    }

    /**
     * Sets a new land
     *
     * @param string $land
     * @return self
     */
    public function setLand($land)
    {
        $this->land = $land;
        return $this;
    }

    /**
     * Gets as stadtbezirk
     *
     * @return string
     */
    public function getStadtbezirk()
    {
        return $this->stadtbezirk;
    }

    /**
     * Sets a new stadtbezirk
     *
     * @param string $stadtbezirk
     * @return self
     */
    public function setStadtbezirk($stadtbezirk)
    {
        $this->stadtbezirk = $stadtbezirk;
        return $this;
    }

    /**
     * Gets as preis
     *
     * @return string
     */
    public function getPreis()
    {
        return $this->preis;
    }

    /**
     * Sets a new preis
     *
     * @param string $preis
     * @return self
     */
    public function setPreis($preis)
    {
        $this->preis = $preis;
        return $this;
    }

    /**
     * Gets as gebot
     *
     * @return string
     */
    public function getGebot()
    {
        return $this->gebot;
    }

    /**
     * Sets a new gebot
     *
     * @param string $gebot
     * @return self
     */
    public function setGebot($gebot)
    {
        $this->gebot = $gebot;
        return $this;
    }

    /**
     * Gets as wae
     *
     * @return string
     */
    public function getWae()
    {
        return $this->wae;
    }

    /**
     * Sets a new wae
     *
     * @param string $wae
     * @return self
     */
    public function setWae($wae)
    {
        $this->wae = $wae;
        return $this;
    }

    /**
     * Gets as anzahlZimmer
     *
     * @return string
     */
    public function getAnzahlZimmer()
    {
        return $this->anzahlZimmer;
    }

    /**
     * Sets a new anzahlZimmer
     *
     * @param string $anzahlZimmer
     * @return self
     */
    public function setAnzahlZimmer($anzahlZimmer)
    {
        $this->anzahlZimmer = $anzahlZimmer;
        return $this;
    }

    /**
     * Gets as flaeche
     *
     * @return string
     */
    public function getFlaeche()
    {
        return $this->flaeche;
    }

    /**
     * Sets a new flaeche
     *
     * @param string $flaeche
     * @return self
     */
    public function setFlaeche($flaeche)
    {
        $this->flaeche = $flaeche;
        return $this;
    }

    /**
     * Adds as interessent
     *
     * @return self
     * @param \Zwei14\OpenImmo\Feedback\API\Interessent $interessent
     */
    public function addToInteressent(\Zwei14\OpenImmo\Feedback\API\Interessent $interessent)
    {
        $this->interessent[] = $interessent;
        return $this;
    }

    /**
     * isset interessent
     *
     * @param int|string $index
     * @return bool
     */
    public function issetInteressent($index)
    {
        return isset($this->interessent[$index]);
    }

    /**
     * unset interessent
     *
     * @param int|string $index
     * @return void
     */
    public function unsetInteressent($index)
    {
        unset($this->interessent[$index]);
    }

    /**
     * Gets as interessent
     *
     * @return \Zwei14\OpenImmo\Feedback\API\Interessent[]
     */
    public function getInteressent()
    {
        return $this->interessent;
    }

    /**
     * Sets a new interessent
     *
     * @param \Zwei14\OpenImmo\Feedback\API\Interessent[] $interessent
     * @return self
     */
    public function setInteressent(array $interessent)
    {
        $this->interessent = $interessent;
        return $this;
    }

    /**
     * Adds as feld
     *
     * @return self
     * @param \Zwei14\OpenImmo\Feedback\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType $feld
     */
    public function addToUserDefinedExtend(\Zwei14\OpenImmo\Feedback\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType $feld)
    {
        $this->userDefinedExtend[] = $feld;
        return $this;
    }

    /**
     * isset userDefinedExtend
     *
     * @param int|string $index
     * @return bool
     */
    public function issetUserDefinedExtend($index)
    {
        return isset($this->userDefinedExtend[$index]);
    }

    /**
     * unset userDefinedExtend
     *
     * @param int|string $index
     * @return void
     */
    public function unsetUserDefinedExtend($index)
    {
        unset($this->userDefinedExtend[$index]);
    }

    /**
     * Gets as userDefinedExtend
     *
     * @return \Zwei14\OpenImmo\Feedback\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[]
     */
    public function getUserDefinedExtend()
    {
        return $this->userDefinedExtend;
    }

    /**
     * Sets a new userDefinedExtend
     *
     * @param \Zwei14\OpenImmo\Feedback\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[] $userDefinedExtend
     * @return self
     */
    public function setUserDefinedExtend(array $userDefinedExtend)
    {
        $this->userDefinedExtend = $userDefinedExtend;
        return $this;
    }


}

