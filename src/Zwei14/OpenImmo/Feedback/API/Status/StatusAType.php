<?php

namespace Zwei14\OpenImmo\Feedback\API\Status;

/**
 * Class representing StatusAType
 */
class StatusAType
{

    /**
     * @var int $statusnr
     */
    private $statusnr = null;

    /**
     * @var string $text
     */
    private $text = null;

    /**
     * Gets as statusnr
     *
     * @return int
     */
    public function getStatusnr()
    {
        return $this->statusnr;
    }

    /**
     * Sets a new statusnr
     *
     * @param int $statusnr
     * @return self
     */
    public function setStatusnr($statusnr)
    {
        $this->statusnr = $statusnr;
        return $this;
    }

    /**
     * Gets as text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Sets a new text
     *
     * @param string $text
     * @return self
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }


}

