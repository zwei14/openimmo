<?php

namespace Zwei14\OpenImmo\Feedback\API\OpenimmoFeedback;

/**
 * Class representing OpenimmoFeedbackAType
 */
class OpenimmoFeedbackAType
{

    /**
     * @var string $version
     */
    private $version = null;

    /**
     * @var \Zwei14\OpenImmo\Feedback\API\Sender $sender
     */
    private $sender = null;

    /**
     * @var \Zwei14\OpenImmo\Feedback\API\Objekt[] $objekt
     */
    private $objekt = [
        
    ];

    /**
     * @var \Zwei14\OpenImmo\Feedback\API\Fehlerliste\FehlerlisteAType\FehlerAType[] $fehlerliste
     */
    private $fehlerliste = null;

    /**
     * @var \Zwei14\OpenImmo\Feedback\API\Status[] $status
     */
    private $status = [
        
    ];

    /**
     * Gets as version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Sets a new version
     *
     * @param string $version
     * @return self
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * Gets as sender
     *
     * @return \Zwei14\OpenImmo\Feedback\API\Sender
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Sets a new sender
     *
     * @param \Zwei14\OpenImmo\Feedback\API\Sender $sender
     * @return self
     */
    public function setSender(\Zwei14\OpenImmo\Feedback\API\Sender $sender)
    {
        $this->sender = $sender;
        return $this;
    }

    /**
     * Adds as objekt
     *
     * @return self
     * @param \Zwei14\OpenImmo\Feedback\API\Objekt $objekt
     */
    public function addToObjekt(\Zwei14\OpenImmo\Feedback\API\Objekt $objekt)
    {
        $this->objekt[] = $objekt;
        return $this;
    }

    /**
     * isset objekt
     *
     * @param int|string $index
     * @return bool
     */
    public function issetObjekt($index)
    {
        return isset($this->objekt[$index]);
    }

    /**
     * unset objekt
     *
     * @param int|string $index
     * @return void
     */
    public function unsetObjekt($index)
    {
        unset($this->objekt[$index]);
    }

    /**
     * Gets as objekt
     *
     * @return \Zwei14\OpenImmo\Feedback\API\Objekt[]
     */
    public function getObjekt()
    {
        return $this->objekt;
    }

    /**
     * Sets a new objekt
     *
     * @param \Zwei14\OpenImmo\Feedback\API\Objekt[] $objekt
     * @return self
     */
    public function setObjekt(array $objekt)
    {
        $this->objekt = $objekt;
        return $this;
    }

    /**
     * Adds as fehler
     *
     * @return self
     * @param \Zwei14\OpenImmo\Feedback\API\Fehlerliste\FehlerlisteAType\FehlerAType $fehler
     */
    public function addToFehlerliste(\Zwei14\OpenImmo\Feedback\API\Fehlerliste\FehlerlisteAType\FehlerAType $fehler)
    {
        $this->fehlerliste[] = $fehler;
        return $this;
    }

    /**
     * isset fehlerliste
     *
     * @param int|string $index
     * @return bool
     */
    public function issetFehlerliste($index)
    {
        return isset($this->fehlerliste[$index]);
    }

    /**
     * unset fehlerliste
     *
     * @param int|string $index
     * @return void
     */
    public function unsetFehlerliste($index)
    {
        unset($this->fehlerliste[$index]);
    }

    /**
     * Gets as fehlerliste
     *
     * @return \Zwei14\OpenImmo\Feedback\API\Fehlerliste\FehlerlisteAType\FehlerAType[]
     */
    public function getFehlerliste()
    {
        return $this->fehlerliste;
    }

    /**
     * Sets a new fehlerliste
     *
     * @param \Zwei14\OpenImmo\Feedback\API\Fehlerliste\FehlerlisteAType\FehlerAType[] $fehlerliste
     * @return self
     */
    public function setFehlerliste(array $fehlerliste)
    {
        $this->fehlerliste = $fehlerliste;
        return $this;
    }

    /**
     * Adds as status
     *
     * @return self
     * @param \Zwei14\OpenImmo\Feedback\API\Status $status
     */
    public function addToStatus(\Zwei14\OpenImmo\Feedback\API\Status $status)
    {
        $this->status[] = $status;
        return $this;
    }

    /**
     * isset status
     *
     * @param int|string $index
     * @return bool
     */
    public function issetStatus($index)
    {
        return isset($this->status[$index]);
    }

    /**
     * unset status
     *
     * @param int|string $index
     * @return void
     */
    public function unsetStatus($index)
    {
        unset($this->status[$index]);
    }

    /**
     * Gets as status
     *
     * @return \Zwei14\OpenImmo\Feedback\API\Status[]
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets a new status
     *
     * @param \Zwei14\OpenImmo\Feedback\API\Status[] $status
     * @return self
     */
    public function setStatus(array $status)
    {
        $this->status = $status;
        return $this;
    }


}

