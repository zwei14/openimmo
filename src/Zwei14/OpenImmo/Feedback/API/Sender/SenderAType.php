<?php

namespace Zwei14\OpenImmo\Feedback\API\Sender;

/**
 * Class representing SenderAType
 */
class SenderAType
{

    /**
     * @var string $name
     */
    private $name = null;

    /**
     * @var string $openimmoAnid
     */
    private $openimmoAnid = null;

    /**
     * @var string $datum
     */
    private $datum = null;

    /**
     * @var string $maklerId
     */
    private $maklerId = null;

    /**
     * @var string $regiId
     */
    private $regiId = null;

    /**
     * Gets as name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets a new name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Gets as openimmoAnid
     *
     * @return string
     */
    public function getOpenimmoAnid()
    {
        return $this->openimmoAnid;
    }

    /**
     * Sets a new openimmoAnid
     *
     * @param string $openimmoAnid
     * @return self
     */
    public function setOpenimmoAnid($openimmoAnid)
    {
        $this->openimmoAnid = $openimmoAnid;
        return $this;
    }

    /**
     * Gets as datum
     *
     * @return string
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * Sets a new datum
     *
     * @param string $datum
     * @return self
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;
        return $this;
    }

    /**
     * Gets as maklerId
     *
     * @return string
     */
    public function getMaklerId()
    {
        return $this->maklerId;
    }

    /**
     * Sets a new maklerId
     *
     * @param string $maklerId
     * @return self
     */
    public function setMaklerId($maklerId)
    {
        $this->maklerId = $maklerId;
        return $this;
    }

    /**
     * Gets as regiId
     *
     * @return string
     */
    public function getRegiId()
    {
        return $this->regiId;
    }

    /**
     * Sets a new regiId
     *
     * @param string $regiId
     * @return self
     */
    public function setRegiId($regiId)
    {
        $this->regiId = $regiId;
        return $this;
    }


}

