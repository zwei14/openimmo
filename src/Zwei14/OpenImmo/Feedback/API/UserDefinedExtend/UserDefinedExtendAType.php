<?php

namespace Zwei14\OpenImmo\Feedback\API\UserDefinedExtend;

/**
 * Class representing UserDefinedExtendAType
 */
class UserDefinedExtendAType
{

    /**
     * @var \Zwei14\OpenImmo\Feedback\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[] $feld
     */
    private $feld = [
        
    ];

    /**
     * Adds as feld
     *
     * @return self
     * @param \Zwei14\OpenImmo\Feedback\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType $feld
     */
    public function addToFeld(\Zwei14\OpenImmo\Feedback\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType $feld)
    {
        $this->feld[] = $feld;
        return $this;
    }

    /**
     * isset feld
     *
     * @param int|string $index
     * @return bool
     */
    public function issetFeld($index)
    {
        return isset($this->feld[$index]);
    }

    /**
     * unset feld
     *
     * @param int|string $index
     * @return void
     */
    public function unsetFeld($index)
    {
        unset($this->feld[$index]);
    }

    /**
     * Gets as feld
     *
     * @return \Zwei14\OpenImmo\Feedback\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[]
     */
    public function getFeld()
    {
        return $this->feld;
    }

    /**
     * Sets a new feld
     *
     * @param \Zwei14\OpenImmo\Feedback\API\UserDefinedExtend\UserDefinedExtendAType\FeldAType[] $feld
     * @return self
     */
    public function setFeld(array $feld)
    {
        $this->feld = $feld;
        return $this;
    }


}

