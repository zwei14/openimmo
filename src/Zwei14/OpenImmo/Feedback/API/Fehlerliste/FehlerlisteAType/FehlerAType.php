<?php

namespace Zwei14\OpenImmo\Feedback\API\Fehlerliste\FehlerlisteAType;

/**
 * Class representing FehlerAType
 */
class FehlerAType
{

    /**
     * @var string $objektId
     */
    private $objektId = null;

    /**
     * @var int $fehlernr
     */
    private $fehlernr = null;

    /**
     * @var string $text
     */
    private $text = null;

    /**
     * Gets as objektId
     *
     * @return string
     */
    public function getObjektId()
    {
        return $this->objektId;
    }

    /**
     * Sets a new objektId
     *
     * @param string $objektId
     * @return self
     */
    public function setObjektId($objektId)
    {
        $this->objektId = $objektId;
        return $this;
    }

    /**
     * Gets as fehlernr
     *
     * @return int
     */
    public function getFehlernr()
    {
        return $this->fehlernr;
    }

    /**
     * Sets a new fehlernr
     *
     * @param int $fehlernr
     * @return self
     */
    public function setFehlernr($fehlernr)
    {
        $this->fehlernr = $fehlernr;
        return $this;
    }

    /**
     * Gets as text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Sets a new text
     *
     * @param string $text
     * @return self
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }


}

