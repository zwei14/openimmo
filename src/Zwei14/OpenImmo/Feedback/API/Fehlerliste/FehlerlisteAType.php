<?php

namespace Zwei14\OpenImmo\Feedback\API\Fehlerliste;

/**
 * Class representing FehlerlisteAType
 */
class FehlerlisteAType
{

    /**
     * @var \Zwei14\OpenImmo\Feedback\API\Fehlerliste\FehlerlisteAType\FehlerAType[] $fehler
     */
    private $fehler = [
        
    ];

    /**
     * Adds as fehler
     *
     * @return self
     * @param \Zwei14\OpenImmo\Feedback\API\Fehlerliste\FehlerlisteAType\FehlerAType $fehler
     */
    public function addToFehler(\Zwei14\OpenImmo\Feedback\API\Fehlerliste\FehlerlisteAType\FehlerAType $fehler)
    {
        $this->fehler[] = $fehler;
        return $this;
    }

    /**
     * isset fehler
     *
     * @param int|string $index
     * @return bool
     */
    public function issetFehler($index)
    {
        return isset($this->fehler[$index]);
    }

    /**
     * unset fehler
     *
     * @param int|string $index
     * @return void
     */
    public function unsetFehler($index)
    {
        unset($this->fehler[$index]);
    }

    /**
     * Gets as fehler
     *
     * @return \Zwei14\OpenImmo\Feedback\API\Fehlerliste\FehlerlisteAType\FehlerAType[]
     */
    public function getFehler()
    {
        return $this->fehler;
    }

    /**
     * Sets a new fehler
     *
     * @param \Zwei14\OpenImmo\Feedback\API\Fehlerliste\FehlerlisteAType\FehlerAType[] $fehler
     * @return self
     */
    public function setFehler(array $fehler)
    {
        $this->fehler = $fehler;
        return $this;
    }


}

