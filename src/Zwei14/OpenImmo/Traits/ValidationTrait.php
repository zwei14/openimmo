<?php

namespace Zwei14\OpenImmo\Traits;

use DOMDocument;

Trait ValidationTrait
{
//    protected function getRecursiveXsdRuleViolations(string $pathToVendorDirectory, object $objectToValidate)
//    {
//        $recursiveViolations = [];
//
//        if (substr(rtrim($pathToVendorDirectory, "/"), -6, 6) !== "vendor") {
//            throw new \Exception('Provide correct path to vendor.');
//        }
//
//        $validationBuilder = Validation::createValidatorBuilder();
//        $glob = glob(rtrim($pathToVendorDirectory, "/") . '/zwei14/openimmo/validation/Zwei14/OpenImmo/API/*.yml');
//        foreach ($glob as $file) {
//            $validationBuilder->addYamlMapping($file);
//        }
//        $validator =  $validationBuilder->getValidator();
//
//        // validate $object
//        $violations = $this->getXsdRuleViolations($pathToVendorDirectory, $objectToValidate);
//
//        return $violations;
//    }
//
//    protected function getXsdRuleViolations(string $pathToVendorDirectory, object $objectToValidate)
//    {
//        if (substr(rtrim($pathToVendorDirectory, "/"), -6, 6) !== "vendor") {
//            throw new \Exception('Provide correct path to vendor.');
//        }
//
//        $validationBuilder = Validation::createValidatorBuilder();
//        $glob = glob(rtrim($pathToVendorDirectory, "/") . '/zwei14/openimmo/validation/Zwei14/OpenImmo/API/*.yml');
//        foreach ($glob as $file) {
//            $validationBuilder->addYamlMapping($file);
//        }
//        $validator =  $validationBuilder->getValidator();
//
//        // validate $object
//        $violations = $validator->validate($objectToValidate, null, ['xsd_rules']);
//
//        return $violations;
//    }

    protected function isXmlValid(string $xml, string $xmlSchemaFile)
    {
        $document = new DOMDocument();
        $document->loadXML($xml);

        try {
            $document->schemaValidate($xmlSchemaFile);
        } catch (\Exception $e) {
            throw new \Exception($e);
            return false;
        }

        return true;
    }
}