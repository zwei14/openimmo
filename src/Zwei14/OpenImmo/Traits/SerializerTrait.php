<?php

namespace Zwei14\OpenImmo\Traits;

use GoetasWebservices\Xsd\XsdToPhpRuntime\Jms\Handler\BaseTypesHandler;
use GoetasWebservices\Xsd\XsdToPhpRuntime\Jms\Handler\XmlSchemaDateHandler;
use JMS\Serializer\Handler\HandlerRegistryInterface;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\Serializer as JmsSerializer;

Trait SerializerTrait
{

    /**
     * @param string $pathToVendorDirectory
     *
     * @return JmsSerializer
     * @throws \Exception
     */
    protected function getOpenImmoJmsSerializer(string $pathToVendorDirectory) :JmsSerializer
    {
        if (substr(rtrim($pathToVendorDirectory, "/"), -6, 6) !== "vendor") {
            throw new \Exception('Provide correct path to vendor.');
        }

        $serializerBuilder = SerializerBuilder::create();
        $serializerBuilder->addMetadataDir(
            rtrim($pathToVendorDirectory, "/") . '/zwei14/openimmo/metadata/Zwei14/OpenImmo/API',
            'Zwei14\OpenImmo\API'
        );
        $serializerBuilder->configureHandlers(function (HandlerRegistryInterface $handler) use ($serializerBuilder) {
            $serializerBuilder->addDefaultHandlers();
            $handler->registerSubscribingHandler(new BaseTypesHandler()); // XMLSchema List handling
            $handler->registerSubscribingHandler(new XmlSchemaDateHandler()); // XMLSchema date handling
        });

        $serializer = $serializerBuilder->build();

        return $serializer;
    }


    /**
     * @param string $pathToVendorDirectory
     *
     * @return JmsSerializer
     * @throws \Exception
     */
    protected function getOpenImmoFeedbackJmsSerializer(string $pathToVendorDirectory) :JmsSerializer
    {
        if (substr(rtrim($pathToVendorDirectory, "/"), -6, 6) !== "vendor") {
            throw new \Exception('Provide correct path to vendor.');
        }

        $serializerBuilder = SerializerBuilder::create();
        $serializerBuilder->addMetadataDir(
            rtrim($pathToVendorDirectory, "/") . '/zwei14/openimmo/metadata/Zwei14/OpenImmo/Feedback/API',
            'Zwei14\OpenImmo\Feedback\API'
        );
        $serializerBuilder->configureHandlers(function (HandlerRegistryInterface $handler) use ($serializerBuilder) {
            $serializerBuilder->addDefaultHandlers();
            $handler->registerSubscribingHandler(new BaseTypesHandler()); // XMLSchema List handling
            $handler->registerSubscribingHandler(new XmlSchemaDateHandler()); // XMLSchema date handling
        });

        $serializer = $serializerBuilder->build();

        return $serializer;
    }
}